<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();

            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php
                    // Post thumbnail.
                    twentyfifteen_post_thumbnail();
                    ?>

                    <header class="entry-header">
                        <?php
                        if ( is_single() ) :
                            the_title( '<h1 class="entry-title">', '</h1>' );
                        else :
                            the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                        endif;
                        ?>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <?php
                        /* translators: %s: Name of current post */
                        global $post, $wpdb;
                        $result = $wpdb->get_results('SELECT declaration_desc, price, declaration_images FROM list_am_declarations WHERE post_id = '.$post->ID);
                        $images = json_decode($result[0]->declaration_images);
                        ?>
                        <div class = "declaration_slider_content">
                            <ul class = "bxslider">
                                <?php
                                foreach ($images as $img){
                                    ?>
                                    <li><img src = "<?= $img; ?>"></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <div id="bx-pager">
                                <?php
                                for ($i = 0;$i<count($images);$i++){
                                    ?>
                                    <a href="" data-slide-index = "<?php echo $i; ?>"><img src = "<?php echo $images[$i]; ?>" width = "46" height = "34"></a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <p class = "declaration_description"><?php echo $result[0]->declaration_desc; ?></p>
                        <p class = "declaration_price"><?php echo $result[0]->price; ?></p>

                        <?php
                        $additional_fields = $wpdb->get_results('SELECT field_name, selected_value FROM list_am_post_additional_fields WHERE post_id = '.$post->ID);
                        foreach ($additional_fields as $additional_field){
                            $add_field_key = $additional_field->field_name;
                            $add_field_label = $wpdb->get_var("SELECT label FROM list_am_post_type_fields WHERE field_name = '".$add_field_key."'");
                            $add_field = $additional_field->selected_value;
                            if ($_SESSION['additional_form_fields'][$add_field_key] == 'map'){
                                $map_id = 'map_'.uniqid();
                                $coords = explode(',',$add_field);
                                ?>
                                <p>
                                    <?php echo '<b>'.$add_field_label.'<b>'; ?>
                                </p>
                                <div id = "<?php echo $map_id; ?>" class = "google_map_field"></div>
                                <?php
                                echo '<script class = "google_map_field_script">
                            google.maps.event.addDomListener(window, "load", function(){
                                mapCanvas = document.getElementById("'.$map_id.'");
                                mapOptions = {
                                    center: {lat: '.$coords[0].', lng: '.$coords[1].'},
                                    zoom: 10,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                                map = new google.maps.Map(mapCanvas, mapOptions);

                                mapMarker = new google.maps.Marker({
                                    position: {lat: '.$coords[0].', lng: '.$coords[1].'},
                                    map: map
                                });
                            });</script>';
                            }
                            else{
                                ?>
                                <p>
                                    <?php echo '<b>'.$add_field_label.'</b>' .': '. $add_field ?>
                                </p>
                                <?php
                            }
                        }
                        wp_link_pages( array(
                            'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
                            'after'       => '</div>',
                            'link_before' => '<span>',
                            'link_after'  => '</span>',
                            'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
                            'separator'   => '<span class="screen-reader-text">, </span>',
                        ) );
                        ?>
                    </div><!-- .entry-content -->

                    <?php
                    // Author bio.
                    if ( is_single() && get_the_author_meta( 'description' ) ) :
                        get_template_part( 'author-bio' );
                    endif;
                    ?>

                    <footer class="entry-footer">
                        <?php twentyfifteen_entry_meta(); ?>
                        <?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
                    </footer><!-- .entry-footer -->
                </article>
            <?php

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

            // Previous/next post navigation.
            the_post_navigation( array(
                'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
            ) );

            // End the loop.
        endwhile;
        ?>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
