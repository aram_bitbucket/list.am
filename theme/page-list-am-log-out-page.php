<?php
if (!session_id())
    session_start();

$_SESSION['list_am_user_state'] = 'logged_out';
if (is_user_logged_in()){
    wp_logout();
}
else if (isset($_SESSION['fb_user_id'])){
    unset($_SESSION['fb_user_id']);
}
wp_redirect(home_url());
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php
get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                // Post thumbnail.
                twentyfifteen_post_thumbnail();
                ?>

                <div class="entry-content">
                </div>
            </article>

        <?php
        endwhile;
        ?>
    </main>
</div><!-- .content-area -->

<?php get_footer(); ?>
