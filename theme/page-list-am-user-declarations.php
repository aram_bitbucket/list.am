<?php

/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

$my_post_meta = array('additional_fields', 'declaration_images', 'price', 'declaration_desc', 'declaration_title');
if (isset($_GET['action'])) {
    global $wpdb;
    if ($_GET['action'] == 'delete') {
        if (isset($_GET['post_id'])) {
            $post_id = $_GET['post_id'];
            //delete post image
            $post_row = $wpdb->get_row('SELECT declaration_images FROM list_am_declarations WHERE post_id = '.$post_id, ARRAY_N);
            $post_image = json_decode($post_row[0], true);
            $upload_dir = wp_upload_dir();
            foreach ($post_image as $img){
                $img_parts = explode("/", $img);
                $img_subdir = implode('/',array($img_parts[6],$img_parts[7],$img_parts[8]));
                $deleted_image_path = $upload_dir['basedir'].'/'.$img_subdir.'/'.$img_parts[count($img_parts)-1];
                unlink($deleted_image_path);
            }

            //delete post
            wp_delete_post($post_id);
            foreach ($my_post_meta as $post_meta) {
                delete_post_meta($post_id, $post_meta);
            }
            $wpdb->delete('list_am_declarations', array('post_id' => $post_id));
            wp_redirect(get_site_url() . '/list-am-user-declarations/');
        }
    }
}

function list_am_user_has_posts()
{
    global $wpdb;
    //get user id
    $user_posts = array();
    if (isset($_SESSION['fb_user_id'])) {
        $row = $wpdb->get_row("SELECT user_id FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = '" . $_SESSION['fb_user_id'] . "'");
        $user_id = $row->user_id;
    } else {
        $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
    }

    $my_custom_post_types = get_list_am_post_types();
    for ($i = 0; $i < count($my_custom_post_types); $i++) {
        $my_posts = get_posts(array('author' => $user_id, 'post_type' => $my_custom_post_types[$i]));
        if (!empty($my_posts)){
            $user_posts[] = $my_posts;
        }
    }

    $merged_id = get_user_meta($user_id, 'merged_accounts', true);
    if (!empty($merged_id)) {
        for ($i = 0; $i < count($my_custom_post_types); $i++) {
            $my_posts = get_posts(array('author' => $merged_id, 'post_type' => $my_custom_post_types[$i]));
            if (!empty($my_posts)){
                $user_posts[] = $my_posts;
            }
        }
    }

    echo '<style>
                .declaration_single_image{  display: block;  }
                .declaration_slider_content {display: none;}
            </style>';

    return $user_posts;
}

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        $list_am_posts = list_am_user_has_posts();
        if (!empty($list_am_posts)) {
            foreach ($list_am_posts as $category_post) {
                // Include the page content template.
                foreach ($category_post as $my_post) {
                    ?>
                    <article id="post-<?php echo $my_post->ID; ?>" <?php post_class(); ?>>
                        <div class="entry-content">
                            <h2><a href="<?php echo $my_post->guid; ?>"><?php echo $my_post->post_title; ?></a></h2>

                            <p><?php echo $my_post->post_content; ?></p>
                        </div>
                        <div class="entry-footer" style="display: block;">
                            <a href="<?php echo get_site_url() . '/declaration-add-form/?post_id=' . $my_post->ID ?>"
                               class="my_post_edit_link">Edit post</a>
                            <a href="<?php echo get_site_url() . '/list-am-user-declarations/?action=delete&post_id=' . $my_post->ID ?>"
                               class="my_post_delete_link">Delete post</a>
                        </div>
                    </article>

                    <?php
                }
            }
        } else {
            ?>
            <p style="margin-left: 30px;">You have no declarations yet</p>
            <?php
        }
        ?>
    </main>
</div><!-- .content-area -->

<?php get_footer(); ?>
