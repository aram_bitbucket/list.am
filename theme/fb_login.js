window.fbAsyncInit = function() {
    FB.init({
        appId      : '1460966724205331',
        cookie     : true,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v1.0' // use version 1.0
    });
};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function statusChangeCallback(response) {
    if (response.status === 'connected') {
        getRegisteredUserData();
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

var list_am_user_data = [];
function getRegisteredUserData() {
    FB.api('/me?fields=name,email,birthday', function(response) {
        list_am_user_data = [response.name, response.id, response.email, response.birthday];
        wp_admin_ajax_url = localStorage.getItem('site_url') + '/wp-admin/admin-ajax.php';
        $.ajax({
            url:wp_admin_ajax_url,
            method: "POST",
            data: {
                "action": 'register_list_am_user',
                "user_data": JSON.stringify(list_am_user_data)
            }
        }).success(function(data){
            if (data == 'list-am-merge-accounts0'){
               location.href = localStorage.getItem('site_url') + '/list-am-user-declarations';
            }
            else{
                location.href = localStorage.getItem('site_url');
            }
        }).error(function(message){
            console.log(message);
        })
    });
}
