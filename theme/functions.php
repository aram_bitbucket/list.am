<?php
if (!session_id())
    session_start();

$plugin_dir_path = WP_PLUGIN_DIR.'/list.am/classes/filters/';
require_once($plugin_dir_path.'RadioFilter.php');
require_once($plugin_dir_path.'RangeFilter.php');
require_once($plugin_dir_path.'SelectFilter.php');
require_once($plugin_dir_path.'CheckboxFilter.php');

function get_list_am_post_types()
{
    global $wpdb;
    $post_type_array = array();
    $post_types = $wpdb->get_results('SELECT post_type_name FROM list_am_post_types', ARRAY_N);
    foreach ($post_types as $post_type) {
        $post_type_array[] = strtolower($post_type[0]);
    }
    return $post_type_array;
}

function get_list_am_field_filters(){
    global $wpdb;
    $field_filters = $wpdb->get_results('SELECT field_name, filter_name FROM list_am_post_type_fields');
    foreach ($field_filters as $obj_key => $obj){
        $field_filters[$obj->field_name] = $obj->filter_name;
        unset($field_filters[$obj_key]);
    }
    return $field_filters;
}

$field_filters = get_list_am_field_filters();

function add_resources()
{
    wp_enqueue_style('style', get_stylesheet_uri());
}

function show_form_fields()
{
    if (isset($_POST['post_type'])) {

        global $wpdb;
        if (isset($_SESSION['additional_form_fields'])) {
            unset($_SESSION['additional_form_fields']);
        }

        if (isset($_SESSION['additional_form_field_content'])) {
            unset($_SESSION['additional_form_field_content']);
        }

        $result = $wpdb->get_results("SELECT * FROM list_am_post_type_fields WHERE post_type_name = '" . $_POST['post_type'] . "'");
        $_SESSION['post_type'] = $_POST['post_type'];
        if (!empty($result)) {
            foreach ($result as $item) {
                $_SESSION['additional_form_fields'][$item->field_name] = $item->field_type;
                //turn field content to array
                if ($item->field_type == 'select' || $item->field_type == 'radio') {
                    $item->field_content = explode(",", $item->field_content);
                    $_SESSION['additional_form_field_content'][$item->field_name] = $item->field_content;
                }
            }
            $_SESSION['additional_form_field_values'] = $result;
            echo 'Data';
        }
        else {
            echo "No data";
        }
    }
}

function get_additional_fields($post_type)
{
    global $wpdb;
    $result = $wpdb->get_results("SELECT * FROM list_am_post_type_fields WHERE post_type_name = '" . $post_type . "'");
    $_SESSION['post_type'] = $post_type;
    if (!empty($result)) {
        foreach ($result as $item) {
            $_SESSION['additional_form_fields'][$item->field_name] = $item->field_type;
            //turn field content to array
            if ($item->field_type == 'select' || $item->field_type == 'radio') {
                $item->field_content = explode(",", $item->field_content);
                $_SESSION['additional_form_field_content'][$item->field_name] = $item->field_content;
            }
        }
        return $result;
    }
    return array();
}

function register_user_fb()
{
    if (isset($_POST['user_data'])) {
        global $wpdb;
        $user_data = json_decode(str_replace('\\', '', $_POST['user_data']));
        $data = array(
            'display_name' => $user_data[0],
            'user_registered' => date("Y-m-d-h:i:s"),
            'user_email' => $user_data[2]
        );

        if (is_null($wpdb->get_row("SELECT * FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = '" . $user_data[1] . "'"))) {
            $wpdb->insert($wpdb->users, $data);
            $user_id = $wpdb->insert_id;

            $wpdb->insert($wpdb->usermeta, array(
                'user_id' => $user_id,
                'meta_key' => 'user_fb_id',
                'meta_value' => $user_data[1]
            ));

            $wpdb->insert($wpdb->usermeta, array(
                'user_id' => $user_id,
                'meta_key' => 'user_birthday',
                'meta_value' => $user_data[3]
            ));
        } else {
            //user log in
            $_SESSION['list_am_user_state'] = 'logged_in';
            $_SESSION['fb_user_id'] = $user_data[1];

            $fb_user_id_obj = $wpdb->get_row("SELECT user_id FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = " . $_SESSION['fb_user_id']);
            $fb_user_id = $fb_user_id_obj->user_id;

            if (isset($_SESSION['want_to_merge_accounts'])) {
                $wp_user_id = $_SESSION['want_to_merge_accounts'];
                //add user metas
                add_user_meta($wp_user_id, 'merged_accounts', $fb_user_id);
                add_user_meta($fb_user_id, 'merged_accounts', $wp_user_id);
                unset($_SESSION['want_to_merge_accounts']);
            } else {
                if (empty(get_user_meta($fb_user_id, 'merged_accounts', true))) {
                    echo 'list-am-merge-accounts';
                }
            }
            echo 'list-am-merge-accounts';
        }

    }
}

function show_all_post_type_front($query)
{
    global $wpdb;
    if (!is_admin() && $query->is_main_query()) {
        $all_post_types = get_post_types();
        unset($all_post_types['page'], $all_post_types['attachment'], $all_post_types['nav_menu_item'], $all_post_types['revision']);
        $list_am_post_types = get_list_am_post_types();

        if ($query->is_home()) {
            $query->set('post_type', $all_post_types);
        }

        if (is_category() || is_tag()) {
            $query->set('post_type', $list_am_post_types);
        }

        if ($query->is_search()) {
            $query->set('post_type', $list_am_post_types);
        }

        $url_query = parse_url($_SERVER['REQUEST_URI'])['query'];

        $additional_field_names = $wpdb->get_results('SELECT field_name FROM list_am_post_type_fields');
        for ($i = 0;$i<count($additional_field_names);$i++){
            $additional_field_names[] = $additional_field_names[$i]->field_name;
            unset($additional_field_names[$i]);
        }
        $additional_field_names[] = 'price';
        $additional_field_names[] = 's';
        if (isset($url_query) && !empty($url_query)) {
                parse_str($url_query, $output);
                //check if url parameters are one of the post type filed names
                foreach (array_keys($output) as $key){
                    if (in_array($key,$additional_field_names) || in_array(str_replace('_to','',$key),$additional_field_names) || in_array(str_replace('_from','',$key),$additional_field_names)){
                        $valid_url_param = true;
                    }
                    else{
                        $valid_url_param = false;
                        break;
                    }
                }

                if ($valid_url_param) {
                    if (array_keys($output)[0] == 's'){
                        array_shift($output);
                    }

                    $url_sql_query = '';
                    $range_values = array();
                    foreach ($output as $output_key => $output_value){
                        if (empty($output_value)){
                            unset($output[$output_key]);
                        }
                        else {
                            //if range filter
                            if (strpos($output_key, '_to') !== false || strpos($output_key, '_from') !== false){
                                if (strpos($output_key, '_to') !== false){
                                    $output_key_body = str_replace('_to','',$output_key);
                                }
                                else{
                                    $output_key_body = str_replace('_from','',$output_key);
                                }
                                $range_values[] = floatval($output_value);
                                sort($range_values, SORT_NUMERIC);
                                $output[$output_key_body] = $range_values;
                                unset($output[$output_key]);
                            }
                        }
                    }

                    $lastElement = end($output);
                    global $field_filters;
                    foreach ($output as $output_key => $output_value) {
                        $filter_name = ucfirst($field_filters[$output_key]).'Filter';
                        if ($output_key != 'price'){
                            $filter_obj = new $filter_name(array('field_name'=>$output_key,'sel_v'=>$output_value));
                        }
                        else{
                            $filter_obj = new RangeFilter(array('field_name'=>$output_key,'sel_v'=>$output_value));
                        }

                        $filter_query = $filter_obj->getQuery();
                        if (!empty($filter_query)){
                            if ($lastElement == $output_value){
                                $url_sql_query .= $filter_query;
                            }
                            else{
                                $url_sql_query .= $filter_query." OR ";
                            }
                        }

                    }

                    $post_id_sql = 'SELECT post_id, COUNT(*) as cnt FROM list_am_post_additional_fields WHERE '.$url_sql_query.' GROUP BY post_id';
                    $post_objs = $wpdb->get_results($post_id_sql);
                    $post_ids = array();
                    foreach ($post_objs as $post_obj){
                        if ($post_obj->cnt == count($output)){
                            $post_ids[] = $post_obj->post_id;
                        }
                    }

                    if (!empty($post_ids)) {
                        $_SESSION['post_id_array'] = $post_ids;
                        $query->set('post__in', $post_ids);
                    } else {
                        if (empty($query->query_vars['s'])) {
                            $query->set('post__in', array(-1));
                        }
                    }
            }
        }
    }
}

function show_post_content($post_content)
{
    global $post, $pagename;
    $content = '';

    if (!is_page($pagename)) {
        if (is_single($post->ID)) {
            echo '<style>.declaration_single_image{  display: none  } .declaration_slider_content {display: block;}</style>';
        } else {
            echo '<style>
                .declaration_single_image{  display: block;  }
                .declaration_slider_content {display: none;}
            </style>';

            $contentArray = explode('<p class = "declaration_desc">', $post_content);
            $descArray = explode('<p', $contentArray[1] );

            $content =
                $contentArray[0] .                                             // image
                '<p><a href=" ' . esc_url( get_permalink() ) . ' " rel="bookmark" > ' . get_the_title($post->ID) . ' </a></p>' . // title
                '<p class = "declaration_desc">' . $descArray[0] .             // description
                '<p' . $descArray[1] .                                         // price
                '<p' . $descArray[2]  ;                                        // category
            $post_content = $content;

        }
    }

    return $post_content;
}

//login functionality
function my_login_redirect($redirect_to, $request, $user)
{
    global $wpdb;
    if (isset($_POST['pwd'], $_POST['log']) && !empty($_POST['pwd']) && !empty($_POST['log'])) {
        //check if user exists
        $user = get_user_by('login', $_POST['log']);
        if ($user && wp_check_password($_POST['pwd'], $user->data->user_pass, $user->ID)) {
            $_SESSION['list_am_user_state'] = 'logged_in';
            if (isset($_SESSION['list_am_login_error'])) {
                unset($_SESSION['list_am_login_error']);
            }

            if (isset($_SESSION['want_to_merge_accounts'])) {
                $wp_user_id = get_current_user_id();
                $fb_user_id_obj = $wpdb->get_row("SELECT user_id FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = " . $_SESSION['fb_user_id']);
                $fb_user_id = $fb_user_id_obj->user_id;
                //add user metas
                add_user_meta($wp_user_id, 'merged_accounts', $fb_user_id);
                add_user_meta($fb_user_id, 'merged_accounts', $wp_user_id);
                unset($_SESSION['want_to_merge_accounts']);
            }
            wp_redirect(home_url() . '/list-am-user-declarations');
            return home_url() . '/list-am-user-declarations';
        } else {
            //Redirect to login page wrong username/password
            $_SESSION['list_am_login_error'] = 'Wrong username password';
            wp_redirect(home_url() . '/list-am-login');
            return home_url() . '/list-am-login';
        }
    } else {
        //Redirect to login page
        wp_redirect(home_url() . '/list-am-login');
        return home_url() . '/list-am-login';
    }
}

function delete_post_image()
{
    $upload_dir = wp_upload_dir();
    $paths = explode("/", $_POST['image']);
    foreach ($_SESSION['existing_declaration_images'] as $obj_key => $obj) {
        if ($obj == $_POST['image']) {
            unset($_SESSION['existing_declaration_images']->$obj_key);
        }
    }
    $image_url = $upload_dir['basedir'] . '/' . $paths[6] . '/' . $paths[7] . '/' . $paths[8] . '/' . $paths[9];
    $_SESSION['to_be_deleted_images'][] = $image_url;
}

add_filter('login_redirect', 'my_login_redirect', 10, 3);

add_action('wp_ajax_nopriv_get_declaration_form_fields', 'show_form_fields');
add_action('wp_ajax_get_declaration_form_fields', 'show_form_fields');

add_action('wp_ajax_nopriv_register_list_am_user', 'register_user_fb');
add_action('wp_ajax_register_list_am_user', 'register_user_fb');

add_action('wp_ajax_nopriv_delete_post_image', 'delete_post_image');
add_action('wp_ajax_delete_post_image', 'delete_post_image');

add_filter('wp_enqueue_scripts', 'add_resources');
add_filter('show_admin_bar', '__return_false');
add_action('pre_get_posts', 'show_all_post_type_front');
add_action('the_content', 'show_post_content');
