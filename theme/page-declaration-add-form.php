<?php
require_once('classes/SessionClass.php');
require_once('classes/FilterClass.php');
require_once('classes/ValidationClass.php');
define('MAX_UPLOAD_FILE_SIZE', 5);
if (!session_id())
    session_start();

function define_user()
{
    global $wpdb;
    if (SessionClass::get('fb_user_id')) {
        $row = $wpdb->get_row("SELECT user_id FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = '" . SessionClass::get('fb_user_id') . "'");
        $user_id = $row->user_id;
    } else {
        $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
    }
    return $user_id;
}

$upload_dir = wp_upload_dir();


function delete_images($images)
{
    global $upload_dir;
    if (!empty($images)) {
        foreach ($images as $img_url) {
            $img_url_array = explode('/', $img_url);
            $new_img_path = $upload_dir['path'] . '/list_am' . '/' . array_pop($img_url_array);
            unlink($new_img_path);
        }
    }
}

function insertDeclaration()
{
    global $wpdb;
    $declaration_data = array();
    $wp_postmeta_content = array();
    $wp_post_content = '';


    $declaration_data['user_id'] = define_user();

    if (!SessionClass::get('real_post_id')) {
        SessionClass::destroy('existing_declaration_images');
    }

    $wp_postmeta_content['declaration_title'] = $declaration_data['declaration_title'] = isset($_POST['declaration_title']) == true ? $_POST['declaration_title'] : '';
    $wp_postmeta_content['declaration_desc'] = $declaration_data['declaration_desc'] = isset($_POST['declaration_desc']) == true ? $_POST['declaration_desc'] : '';
    if (isset($_POST['declaration_price'], $_POST['price_unit'])) {
        $wp_postmeta_content['price'] = $declaration_data['price'] = $_POST['declaration_price'] . ' ' .  $_POST['price_unit'];
    } else {
        $wp_postmeta_content['price'] = $declaration_data['price'] = '';
    }

    if (SessionClass::get('declaration_images')) {
        //insert post
        $all_images = SessionClass::get('declaration_images');
        if (SessionClass::get('existing_declaration_images')) {
            $existing_img_array = json_decode(json_encode(SessionClass::get('existing_declaration_images')), true);
            $all_images = array_merge($existing_img_array, SessionClass::get('declaration_images'));
        }
    } else if (SessionClass::get('existing_declaration_images')) {
        $all_images = json_decode(json_encode(SessionClass::get('existing_declaration_images')), true);
    }

    $declaration_data['declaration_images'] = json_encode($all_images);

    //fill wp_post_content
    foreach ($all_images as $image_key => $image_src) {
        $wp_post_content .= '<img src = "' . $image_src . '" alt = "' . $image_key . '" class = "declaration_single_image">';
        break;
    }

    $wp_post_content .= '<div class = "declaration_slider_content">';
    $wp_post_content .= '<ul class = "bxslider">';
    foreach ($all_images as $image_key => $image_src) {
        $wp_post_content .= '<li><img src = "' . $image_src . '" alt = "' . $image_key . '"></li>';
    }
    $wp_post_content .= '</ul>';

    $slider_image = -1;
    $wp_post_content .= '<div id="bx-pager">';
    foreach ($all_images as $image_name => $image_src) {
        ++$slider_image;
        $wp_post_content .= '<a data-slide-index="' . $slider_image . '" href=""><img src = "' . $image_src . '" alt = "' . $image_name . '" width = "46" height = "34"></a>';
    }
    $wp_post_content .= '</div>';
    $wp_post_content .= '</div>';

    $wp_post_content .= '<p class = "declaration_desc">' . $declaration_data['declaration_desc'] . '</p>';
    $wp_post_content .= '<p class = "declaration_price">' . $declaration_data['price'] . '</p>';

    $additional_fields = array();

    //for making string of additional fields with multiple mandatory values, such as range
    $post_item_substr = '';

    foreach ($_POST as $post_key => $post_item) {
        if (strpos($post_key, 'additional_field_') !== false) {
            $underscore_count_in_word = substr_count($post_key, '_');
            if (isset($post_item)) {
                if ($underscore_count_in_word < 3) {
                    $additional_fields[] = $post_item;
                    $post_item_substr = '';
                } else if ($underscore_count_in_word == 3) {
                    $post_key_ess_part = substr($post_key, 0, strlen($post_key) - 1);
                    foreach ($_POST as $post_sub_key => $post_sub_item) {
                        if (strpos($post_sub_key, $post_key_ess_part) !== false) {
                            $post_item_substr .= $post_sub_item . ',';
                            unset($_POST[$post_sub_key]);
                        }
                    }
                    if (!empty($post_item_substr)) {
                        $additional_fields[] = substr($post_item_substr, 0, strlen($post_item_substr) - 1);
                        $post_item_substr = '';
                    }
                }
            }
        }
    }


    $additional_fields = array_combine(array_keys(SessionClass::get('additional_form_fields')), $additional_fields);
    //delete not filled custom fields from $_POST
    foreach ($additional_fields as $post_key => $post_item){
            if ($post_item == '' || $post_item == 'None' || $post_item == 'Any' || $post_item == ','){
                unset($additional_fields[$post_key]);
            }
    }
    $declaration_data['additional_fields'] = json_encode($additional_fields, JSON_UNESCAPED_UNICODE);

    foreach ($additional_fields as $addition_field_key => $addition_field_value) {
        $wp_post_content .= '<p>' . $addition_field_key . ': ' . $addition_field_value . '</p>';
    }

    //insert post to wp_posts table
    $post_title = $declaration_data['declaration_title'];
    $declaration_data['post_type'] = str_replace(' ', '', strtolower(SessionClass::get('post_type')));
    if (!SessionClass::get('real_post_id')) {
        $post_id = wp_insert_post(
            array(
                'post_type' => $declaration_data['post_type'],
                'post_content' => $wp_post_content,
                'post_title' => $post_title,
                'post_status' => 'publish',
                'post_category' => array(get_cat_ID(SessionClass::get('post_type'))),
                'post_author' => $declaration_data['user_id']
            ));
    } else {
        $post_id = SessionClass::get('real_post_id');
        wp_update_post(
            array(
                'ID' => intval($post_id),
                'post_type' => $declaration_data['post_type'],
                'post_content' => $wp_post_content,
                'post_title' => $post_title,
                'post_status' => 'publish',
                'post_category' => array(get_cat_ID(SessionClass::get('post_type'))),
                'post_author' => $declaration_data['user_id']
            )
        );
    }

    //insert table to list_am_declarations table
    $declaration_data['post_id'] = $post_id;
    if (SessionClass::get('real_post_id')) {
        $result = $wpdb->update('list_am_declarations', $declaration_data, array('post_id' => intval(SessionClass::get('real_post_id'))));
    } else {
        $wpdb->insert('list_am_declarations', $declaration_data);
    }

    //fill list_am_post_additional_fields table
    if (SessionClass::get('real_post_id')) {
        $wpdb->delete('list_am_post_additional_fields', array('post_id' => intval(SessionClass::get('real_post_id'))));
        foreach ($additional_fields as $add_field_key => $add_field_value) {
            $wpdb->insert('list_am_post_additional_fields', array('post_id' => intval(SessionClass::get('real_post_id')),'field_name' => $add_field_key, 'selected_value' => $add_field_value));
        }
    } else {
        foreach ($additional_fields as $add_field_key => $add_field_value) {
            $wpdb->insert('list_am_post_additional_fields', array(
                'post_id' => $post_id,
                'field_name' => $add_field_key,
                'selected_value' => $add_field_value
            ));
        }

        $wpdb->insert('list_am_post_additional_fields', array(
            'post_id' => $post_id,
            'field_name' => 'price',
            'selected_value' => $_POST['declaration_price']
        ));
    }


    $declaration_data['additional_fields'] = json_encode($additional_fields, JSON_UNESCAPED_UNICODE);

    //insert data to wp_postmeta table
    $wp_postmeta_content['declaration_images'] = $declaration_data['declaration_images'];
    $wp_postmeta_content['additional_fields'] = $declaration_data['additional_fields'];


    foreach ($wp_postmeta_content as $meta_key => $meta_value) {
        if (!SessionClass::get('real_post_id')) {
            $wpdb->insert($wpdb->postmeta, array(
                'post_id' => $post_id,
                'meta_key' => $meta_key,
                'meta_value' => $meta_value
            ));
        } else {
            $wpdb->update($wpdb->postmeta, array(
                'post_id' => $post_id,
                'meta_key' => $meta_key,
                'meta_value' => $meta_value
            ), array('ID' => SessionClass::get('real_post_id')));
        }

    }

    SessionClass::destroy(array('declaration_images', 'existing_declaration_images', 'to_be_deleted_images', 'real_post_id'));

    //delete unnecessary images
    foreach ($_SESSION['to_be_deleted_images'] as $del_img) {
        if (file_exists($del_img)) {
            unlink($del_img);
        }
    }
}

//uploading images
if (!empty($_FILES)) {
    $MAX_UPLOAD_FILE_COUNT = 10;
    if (SessionClass::get('existing_declaration_images')) {
        $ex_array = json_decode(json_encode(SessionClass::get('existing_declaration_images')), true);
        $MAX_UPLOAD_FILE_COUNT = 10 - count($ex_array);
    }

    $upload_until = count($_FILES['declaration_images']['name']) > $MAX_UPLOAD_FILE_COUNT ? $MAX_UPLOAD_FILE_COUNT : count($_FILES['declaration_images']['name']);

    for ($i = 0; $i < $upload_until; $i++) {
        $file_name = $_FILES['declaration_images']['name'][$i];
        $file_parts = explode(".", $file_name);
        $file_size = ceil($_FILES['declaration_images']['size'][$i] / (1024 * 1024));
        $file_ext = $file_parts[count($file_parts) - 1];
        $file_tmp_loc = $_FILES['declaration_images']['tmp_name'][$i];
        $new_loc = $upload_dir['path'] . '/list_am';
        //make folder if it doesn't exist
        if (!file_exists($new_loc)){
            mkdir($new_loc);
        }
        $new_name = uniqid() . '_' . date("Y-m-d") . "." . strtolower($file_ext);
        $is_image = getimagesize($file_tmp_loc);

        if ($is_image !== false && $file_size <= MAX_UPLOAD_FILE_SIZE) {
            ///image is
            $move_to = $new_loc . '/' . $new_name;
            if (move_uploaded_file($file_tmp_loc, $move_to)) {
                $_SESSION['declaration_images'][] = $upload_dir['url'] . '/list_am' . '/' . $new_name;
                SessionClass::destroy('image_error');
            } else {
                SessionClass::set('image_error', 'Images can not be uploaded');
                delete_images(SessionClass::get('declaration_images'));
                return false;
            }
        } else {
            SessionClass::set('image_error', 'Upload images');
            delete_images(SessionClass::get('declaration_images'));
            return false;
        }
    }
}

$declaration_fields = array();
$declaration_field_rules = array(
    'declaration_title' => array(
        'required' => true
    ),
    'declaration_desc' => array(
        'required' => true
    ),
    'declaration_price' => array(
        'required' => true,
        'type' => 'number',
        'length'=>array(
            'min' => 0
        )
    ),
    'price_unit' => array(
        'required' => true,
        'in_array' => array('դրամ', 'դոլար', 'ռուբլի', 'եվրո')
    )
);
if (isset($_POST['declaration_title'], $_SESSION['list_am_user_state']) && $_SESSION['list_am_user_state'] == 'logged_in') {

    //make data for validation;
    foreach ($_POST as $post_key => $post_item) {
        if ($post_key != 'submit_declaration') {
            if (!isset($post_item)) {
                SessionClass::set('error', 'Please fill in all fields');
                if (!empty($declaration_fields)) {
                    $declaration_fields = array();
                }
                return;
            } else {
                if (strpos($post_key, 'additional_field_') !== false) {
                    if (!empty($post_item) && $post_item != 'Any' && $post_item != 'None'){
                        $declaration_fields[$post_key] = $post_item;
                    }
                    $additional_field_keys[$post_key] = $post_key;
                }
                else{
                    $declaration_fields[$post_key] = $post_item;
                }
            }
        }
    }

    //add validation rules
    $additional_field_rules = array();
    foreach ($_SESSION['additional_form_fields'] as $field_name => $field_type) {
        $field_rule = array('required' => false);
        switch ($field_type){
            case 'select':
            case 'radio':
                $field_rule['in_array'] = $_SESSION['additional_form_field_content'][$field_name];
                break;
            case 'range':
                $field_rule['type'] = 'number';
                $field_rule['length'] = array(
                    'min'=>0
                );
                break;
            case 'map':
                $field_rule['type'] = 'number';
                $field_rule['values_count'] = 'multiple';
                break;
        }
        $additional_field_rules[] = $field_rule;
    }

    $field_rule_index = -1;
    foreach ($additional_field_keys as $key) {
        if (!isset($declaration_field_rules[$key])){
            ++$field_rule_index;
            $underscore_count_in_word = substr_count($key, '_');
            if ($underscore_count_in_word == 3) {
                $key_ess_part = substr($key, 0, strrpos($key, '_'));
                foreach ($additional_field_keys as $sub_key) {
                    if (strpos($sub_key, $key_ess_part) !== false) {
                        $declaration_field_rules[$sub_key] = $additional_field_rules[$field_rule_index];
                    }
                }
            } else {
                $declaration_field_rules[$key] = $additional_field_rules[$field_rule_index];
            }
        }
    }

    //remove empty and default data from $declaration_field_rules
    foreach ($declaration_field_rules as $d_key => $d_value){
        if ($_POST[$d_key] == '' || $_POST[$d_key] == 'Any' || $_POST['None']){
           unset($declaration_field_rules[$d_key]);
        }
    }

    $filtered_declaration_fields = FilterClass::filterFormData($declaration_fields);

    if (ValidationClass::is_valid($filtered_declaration_fields, $declaration_field_rules)) {

        SessionClass::destroy(array('fields_before_error', 'error_fields', 'error'));

        //if no image error, insert declararion
        if (!SessionClass::get('image_error')) {
            if (SessionClass::get('declaration_images') || SessionClass::get('existing_declaration_images')) {
                insertDeclaration();
                wp_redirect(home_url() . '/list-am-user-declarations');
            } else {
                SessionClass::set('image_error', 'You must upload at least 1 image');
            }

        } else {
            //delete unnecesarry images
            delete_images(SessionClass::get('declaration_images'));
            SessionClass::destroy('declaration_images');
        }
    } else {
        //delete unnecesarry images
        delete_images(SessionClass::get('declaration_images'));
        SessionClass::destroy('declaration_images');
    }

} else if (isset($_POST['submit_declaration'], $_SESSION['declaration_images']) && !isset($_SESSION['list_am_user_state'])) {
    //user is not authorized
    wp_redirect(get_site_url() . '/list-am-registration/');
}
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<script type="text/javascript">
    var site_url = '<?= get_site_url(); ?>';
    if (localStorage.getItem('site_url') == null) {
        localStorage.setItem("site_url", site_url);
    }
</script>
<?php
get_header(); ?>
<div id="primary" class="content-area" xmlns="http://www.w3.org/1999/html">
    <div class="declaration_add_form_spinner"></div>
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                // Post thumbnail.
                twentyfifteen_post_thumbnail();
                ?>
                <div class="entry-content">
                    <?php
                    $fields_before_error = SessionClass::get('fields_before_error');
                    $error_fields = SessionClass::get('error_fields');
                    $image_error = SessionClass::get('image_error');

                    if ($fields_before_error) {
                        $declaration_title = isset($fields_before_error['declaration_title']) == true ? $fields_before_error['declaration_title'] : "";
                        $declaration_desc = isset($fields_before_error['declaration_desc']) == true ? $fields_before_error['declaration_desc'] : "";
                        $declaration_price = isset($fields_before_error['declaration_price']) == true ? $fields_before_error['declaration_price'] : "";
                        $declaration_price_unit = isset($fields_before_error['price_unit']) == true ? $fields_before_error['price_unit'] : "";
                    } else {
                        $declaration_title = $declaration_desc = $declaration_price = '';
                        $declaration_price_unit = "դրամ";
                    }


                    if ($error_fields) {
                        $declaration_title_error = isset($error_fields['declaration_title']) == true ? $error_fields['declaration_title'] : "";
                        $declaration_desc_error = isset($error_fields['declaration_desc']) == true ? $error_fields['declaration_desc'] : "";
                        $declaration_price_error = isset($error_fields['declaration_price']) == true ? $error_fields['declaration_price'] : "";
                        $declaration_price_unit_error = isset($error_fields['price_unit']) == true ? $error_fields['price_unit'] : "";
                    } else {
                        $declaration_title_error = $declaration_desc_error = $declaration_price_error = $declaration_price_unit_error = "";
                    }

                    ?>
                    <!--                    Declaration edit form-->
                    <?php
                    if (isset($_GET['post_id'])) {
                        require_once('includes/declaration_edit_form.php');
                    }
                    else {
                        //add form
                        require_once('includes/declaration_add_form.php');
                    }
                    ?>
                    <p class="list_am_registration_form_error"
                       style="clear:both;"><?php echo $declaration_price_unit_error; ?></p>

                    <p class="list_am_registration_form_error"><?php if (SessionClass::get('error')) echo SessionClass::get('error'); ?></p>
                </div>
                <!-- .entry-content -->

                <?php edit_post_link(__('Edit', 'twentyfifteen'), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->'); ?>

            </article><!-- #post-## -->
            <?php

            // End the loop.
        endwhile;
        ?>

    </main>
    <script type="text/javascript" src="<?php echo get_stylesheet_uri() . '/../spin.js/spin.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_uri() . '/../dropzone/dist/dropzone.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_uri() . '/../dropzone_script.js'; ?>"></script>
    <!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
