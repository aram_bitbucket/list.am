<?php
require_once('classes/SessionClass.php');
require_once('classes/FilterClass.php');
require_once('classes/ValidationClass.php');
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<script type="text/javascript">
    var site_url = '<?= get_site_url(); ?>';
    if (localStorage.getItem('site_url') == null) {
        localStorage.setItem("site_url", site_url);
    }
</script>
<?php
$dataRules = array(
    'firstname' => array(
        'required' => true,
        'type' => 'alpha',
        'length' => array(
            'min' => 2,
            'max' => 20
        )
    ),
    'lastname' => array(
        'required' => true,
        'type' => 'alpha',
        'length' => array(
            'min' => 2,
            'max' => 20
        )
    ),
    'username' => array(
        'required' => true,
        'type' => 'username'
    ),
    'password' => array(
        'required' => true,
        'type' => 'password'
    ),
    'email' => array(
        'required' => true,
        'type' => 'email'
    ),
    'birthday_date' => array(
        'type' => 'date',
        'minAge'=> 8,
        'maxAge'=> 120
    ),
    'user_phone' => array(
        'regexp'=>'/^0[0-9]{8}$/'
    )
);

if (SessionClass::get('error')) {
    SessionClass::destroy('error');
}

if (isset($_POST['list_am_registration_submit']) && !empty($_POST['list_am_registration_submit'])) {
    if (isset($_POST['firstname'], $_POST['lastname'], $_POST['username'], $_POST['password'], $_POST['email'])) {
        global $wpdb;
        $data = array(
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'password' => $_POST['password']
        );
        if (isset($_POST['birthday_date']) && !empty($_POST['birthday_date'])) {
            $data['birthday_date'] = $_POST['birthday_date'];
        }
        if (isset($_POST['user_phone']) && !empty($_POST['user_phone'])) {
            $data['user_phone'] = $_POST['user_phone'];
        }
        $filtered_data = FilterClass::filterFormData($data);

        if (ValidationClass::is_valid($filtered_data, $dataRules)) {
            if (SessionClass::get('error')) {
                SessionClass::destroy('error');
            }

            $user_id = wp_insert_user(array(
                'user_login' => $filtered_data['username'],
                'user_registered' => date("Y-m-d-h:i:s"),
                'user_email' => $filtered_data['email'],
                'user_pass' => $filtered_data['password'],
                'user_nicename' => $filtered_data['firstname'] . ' ' . $filtered_data['lastname'],
                'display_name' => $filtered_data['firstname'] . ' ' . $filtered_data['lastname']
            ));

            if (!is_wp_error($user_id)) {
                if (isset($filtered_data['birthday_date'])) {
                    $wpdb->insert($wpdb->usermeta, array(
                        'user_id' => $user_id,
                        'meta_key' => 'user_birthday',
                        'meta_value' => $filtered_data['birthday_date']
                    ));
                }

                if (isset($filtered_data['user_phone'])) {
                    $wpdb->insert($wpdb->usermeta, array(
                        'user_id' => $user_id,
                        'meta_key' => 'phone_number',
                        'meta_value' => $filtered_data['user_phone']
                    ));
                }

                $_SESSION['list_am_user_state'] = 'registered';
                header("Location: " . get_site_url().'/list-am-registration/');
            } else {
                SessionClass::set('error', $user_id->get_error_message());
            }
        }
    } else {
        SessionClass::set('error', 'Firstname, lastname, username, password, email fields are required');
    }
}
get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                // Post thumbnail.
                twentyfifteen_post_thumbnail();
                if (isset($_SESSION['list_am_user_state']) && $_SESSION['list_am_user_state'] == 'registered'){
                    ?>
                    <h2>You have succesfully registered</h2>
                    <?php
                }
                else {
                    ?>
                    <header class="entry-header">
                        <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
                    </header>
                    <!-- .entry-header -->
                    <div class="entry-content">
                        <form action="" method="POST" name="list_am_registration">
                            <?php
                            $valid_data = SessionClass::get('fields_before_error');
                            if ($valid_data){
                                $firstname = isset($valid_data['firstname'])?$valid_data['firstname']:"";
                                $lastname = isset($valid_data['lastname'])?$valid_data['lastname']:"";
                                $username = isset($valid_data['username'])?$valid_data['username']:"";
                                $email = isset($valid_data['email'])?$valid_data['email']:"";
                                $password = isset($valid_data['password'])?$valid_data['password']:"";
                                $birthday = isset($valid_data['birthday_date'])?$valid_data['birthday_date']:"";
                                $user_phone = isset($valid_data['user_phone'])?$valid_data['user_phone']:"";
                            }

                            $error_messages = SessionClass::get('error_fields');
                            if ($error_messages){
                                $firstname_error = isset($error_messages['firstname'])?$error_messages['firstname']:"";
                                $lastname_error = isset($error_messages['lastname'])?$error_messages['lastname']:"";
                                $username_error = isset($error_messages['username'])?$error_messages['username']:"";
                                $email_error = isset($error_messages['email'])?$error_messages['email']:"";
                                $password_error = isset($error_messages['password'])?$error_messages['password']:"";
                                $birthday_error = isset($error_messages['birthday_date'])?$error_messages['birthday_date']:"";
                                $user_phone_error = isset($error_messages['user_phone'])?$error_messages['user_phone']:"";
                            }
                            ?>
                            <label for="firstname" style="cursor: pointer">Firstname</label>
                            <input type="text" name="firstname" id="firstname" placeholder="Type letters from to 2 to 20"
                                   class="user_reg_field"  value = "<?php echo $firstname;?>" required/>
                            <p class = "list_am_registration_form_error"><?php echo $firstname_error;  ?></p>
                            <label for="lastname" style="cursor: pointer">Lastname</label>
                            <input type="text" name="lastname" id="lastname"value = "<?php echo $lastname;?>" placeholder="Type letters from to 2 to 20"
                                   class="user_reg_field"
                                   required/>
                            <p class = "list_am_registration_form_error"><?php echo $lastname_error;  ?></p>
                            <label for="username" style="cursor: pointer">Username</label>
                            <input type="text" name="username" id="username" value = "<?php echo $username;?>" placeholder="Type letters, numbers, -_\. and start with letter, from 5 to 30 characters"
                                   class="user_reg_field"
                                   required/>
                            <p class = "list_am_registration_form_error"><?php echo $username_error;  ?></p>
                            <label for="email" style="cursor: pointer">Email</label>
                            <input type="email" name="email" id="email" value = "<?php echo $email;?>" placeholder="Type valid email" class="user_reg_field"
                                   required/>
                            <p class = "list_am_registration_form_error"><?php echo $email_error;  ?></p>
                            <label for="password" style="cursor: pointer">Password</label>
                            <input type="password" name="password"  value = "<?php echo $password;?>" id="password" placeholder="Type string, from 5 to 30 characters"
                                   class="user_reg_field" required/>
                            <p class = "list_am_registration_form_error"><?php echo $password_error;  ?></p>
                            <label for="birthday_date" style="cursor: pointer">Birthday date</label>
                            <input type="date" name="birthday_date" id="birthday_date" placeholder="Birthday date"
                                   class="user_reg_field" value = "<?php echo $birthday;?>"/>
                            <p class = "list_am_registration_form_error"><?php echo $birthday_error;  ?></p>
                            <label for="user_phone" style="cursor: pointer">Phone number</label>
                            <input type="text" name="user_phone" id="user_phone" class="user_reg_field"
                                   placeholder="Type 9 digits starting with 0" value = "<?php echo $user_phone;?>"/>
                            <p class = "list_am_registration_form_error"><?php echo $user_phone_error;  ?></p>
                            <input type="submit" name="list_am_registration_submit" id="register_btn" value="Register"/>
                        </form>
                        <p class="list_am_registration_form_error">
                            <?php if (SessionClass::get('error')) {
                                echo SessionClass::get('error');
                            } ?>
                        </p>

                        <p style="margin-top: 50px; margin-bottom: 20px;">Register via your facebook account</p>
                        <fb:login-button size="xlarge" scope="public_profile,email,user_birthday"
                                         onlogin="checkLoginState();">Register
                        </fb:login-button>

                        <div id="status">
                        </div>
                    </div>
                    <?php
                }
                ?>
            </article>

    <?php
            // End the loop.
        endwhile;
        ?>
    </main>
    <!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
