jQuery(document).ready(function () {
    jQuery(".declaration_categories li.cat-item a").attr('title', '').attr('href', '');

    jQuery(".declaration_categories li.cat-item a").on("click", function (e) {
        e.preventDefault();
        var post_type = e.target.childNodes[0].data;
        var wp_admin_ajax_url = localStorage.getItem('site_url') + '/wp-admin/admin-ajax.php';
        jQuery.ajax({
            url: wp_admin_ajax_url,
            method: "POST",
            data: {
                "action": 'get_declaration_form_fields',
                "post_type": post_type
            }
        }).success(function (data) {
            data = data.substring(0, data.length - 1);
            data = data.replace(/ \\r\\n/g, ',');
            if (data != 'No data') {
                location.href = localStorage.getItem('site_url') + '/declaration-add-form/';
            }
        });
    });

    jQuery('.list_am_filter_radio').click(function () {
        var parent_url = jQuery(this).parent().attr('href');
        location.href = parent_url;
    });
});
