<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
    <!-- Jquery bxslider styles-->
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_stylesheet_uri() . '/../jquery.bxslider/jquery.bxslider.css'; ?>"/>
    <!-- Dropzone styles -->
    <link rel="stylesheet" type="text/css"
          href="<?php echo get_stylesheet_uri() . '/../dropzone/dist/dropzone.css'; ?>"/>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <?php wp_head(); ?>
</head>

<body <?php if (is_page(array('declaration-add-form'))) {
    body_class('declataion_add_form');
} else {
    body_class();
} ?>>

<header id="masthead" class="row site-header" role="banner">
    <div class="site-branding">
        <?php
        if (is_front_page() && is_home()) : ?>
            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"
                                      rel="home"><?php bloginfo('name'); ?></a></h1>
        <?php else : ?>
            <p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"
                                     rel="home"><?php bloginfo('name'); ?></a></p>
        <?php endif;

        $description = get_bloginfo('description', 'display');
        if ($description || is_customize_preview()) : ?>
            <p class="site-description"><?php echo $description; ?></p>
        <?php endif;
        ?>
        <button class="secondary-toggle"><?php _e('Menu and widgets', 'twentyfifteen'); ?></button>
    </div>
    <!-- .site-branding -->

    <div class="right_menu">
        <a class="add_declaration"
           href="<?php if (!isset($_SESSION['list_am_user_state']) || $_SESSION['list_am_user_state'] != 'logged_in') echo get_site_url() . '/login/'; else echo get_site_url() . '/declaration-add-categories/' ?>">Ավելացնել
            հայտարարություն</a>
        <?php
        global $pagename;
        if (!isset($_SESSION['list_am_user_state']) || $_SESSION['list_am_user_state'] != 'logged_in') {
            ?>
            <a class="register_log_in"
               style="<?php if ($pagename == 'list-am-registration') echo 'color:#000'; ?>"
               href="<?php echo get_site_url() . '/list-am-registration' ?>">Register</a>
            <?php
        }

        if (!isset($_SESSION['list_am_user_state']) || $_SESSION['list_am_user_state'] != 'logged_in') {
            ?>
            <a class="register_log_in" style="<?php if ($pagename == 'list-am-login') echo 'color:#000'; ?>"
               href="<?php echo get_site_url() . '/list-am-login' ?>">Log in</a>
            <?php
        } else {
            ?>
            <a class="register_log_in"
               style="<?php if ($pagename == 'list_am_user_profile') echo 'color:#000'; ?>"
               href="<?php echo get_site_url() . '/list_am_user_profile' ?>">My Profile</a>
            <a class="register_log_in"
               style="<?php if ($pagename == 'list-am-user-declarations') echo 'color:#000'; ?>"
               href="<?php echo get_site_url() . '/list-am-user-declarations' ?>">My Posts</a>
            <a class="register_log_in"
               style="<?php if ($pagename == 'list-am-log-out-page') echo 'color:#000'; ?>"
               href="<?php echo get_site_url() . '/list-am-log-out-page'; ?>">Logout</a>
            <?php
        }
        ?>
    </div>
</header>
<!-- .site-header -->
<div id="page" class="hfeed site">
    <a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'twentyfifteen'); ?></a>

    <div id="sidebar" class="sidebar">
        <?php get_sidebar(); ?>
    </div>
    <!-- .sidebar -->

    <div id="content" class="site-content">
