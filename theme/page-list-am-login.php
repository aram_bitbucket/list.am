<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
require_once('classes/SessionClass.php');
SessionClass::destroy('want_to_merge_accounts');
?>
<script type="text/javascript">
    var site_url = '<?= get_site_url(); ?>';
    if (localStorage.getItem('site_url') != null) {
        localStorage.setItem("site_url", site_url);
    }
</script>
<?php
get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                // Post thumbnail.
                twentyfifteen_post_thumbnail();
                ?>

                <div class="entry-content">
                    <?php
                    wp_login_form(
                        array(
                            'redirect' => home_url(),
                            'remember'=>false
                        )
                    ); ?>
                    <p><?php if (isset($_SESSION['list_am_login_error'])) echo $_SESSION['list_am_login_error']; ?></p>
                    <fb:login-button size="xlarge" scope="public_profile,email,user_birthday"
                                     onlogin="checkLoginState();">Log in
                    </fb:login-button>
                    <div id="status"></div>
                </div>
            </article>

        <?php
        endwhile;
        ?>
    </main>
</div><!-- .content-area -->

<?php get_footer(); ?>
