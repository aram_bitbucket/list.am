<?php
if (!session_id())
session_start();

class SessionClass{
	
	public static function set($key,$value){
		$_SESSION[$key] = $value;
	}

	public static function get($key){
		if (isset($_SESSION[$key])){
			return $_SESSION[$key];
		}
		return false;
		
	}

	public static function destroy($keys){
		if (!is_array($keys)){
			if (isset($_SESSION[$keys])){
				unset($_SESSION[$keys]);
			}
		}
		else{
			foreach ($keys as $key){
				if (isset($_SESSION[$key])){
					unset($_SESSION[$key]);
				}
			}
		}
	}

	public static function destroyAll(){
		session_destroy();
	}
}