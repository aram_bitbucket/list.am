<?php
require_once('SessionClass.php');
class ValidationClass{
	private static $data, $rules;
    private static $letters = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
	private static $characters = array('_','-',',','"','<','>','#','@','!','$','%','&',' ');
    private static $numbers = array('0','1','2','3','4','5','6','7','8','9');
    private static $fields_before_error = array();
    private static $error_fields = array();
    public static function is_valid($data, $rules){
		self::$data = $data;
        self::$rules = $rules;
        SessionClass::destroy(array('error_fields','fields_before_error'));
        foreach (self::$data as $key=>$dataItem){
            $rule = self::$rules[$key];
            if (!is_null($rule)){
                if (isset($rule['required'])){
                    if ($rule['required']){
                        if (!self::checkEmpty($dataItem)){
                            self::addInValidFieldToSession($key, "Field is required");
                        }
                        else{
                            self::addValidField($key, $dataItem);
                        }
                    }
                }

                if (isset($rule['type'])){
                    switch ($rule['type'] ){
                        case 'number':
                            if (isset($rule['values_count'])){
                                switch ($rule['values_count']){
                                    case 'multiple':
                                        $multiple_parts = explode(',',$dataItem);
                                        foreach ($multiple_parts as $part){
                                            if (!self::checkIsNumber($part)){
                                                self::addInValidFieldToSession($key, 'Not valid data specified');
                                            }
                                        }
                                        if (!isset(self::$error_fields[$key])) {
                                            self::addValidField($key, $dataItem);
                                        }
                                        break;
                                }
                            }
                            else{
                                if (!self::checkIsNumber($dataItem)){
                                    self::addInValidFieldToSession($key, 'Not valid data specified');
                                }
                                else{
                                    self::addValidField($key, $dataItem);
                                }
                            }

                            break;
                        case 'alpha':
                            if (!self::checkIsAlpha($dataItem)){
                                self::addInValidFieldToSession($key, 'Field must contain only alphabetic characters');
                            }
                            else{
                                self::addValidField($key, $dataItem);
                            }
                            break;
                        case 'date':
                            $userMinAge = null;
                            $userMaxAge = null;
                            if (isset($rule['minAge'])){
                                $userMinAge = $rule['minAge'];
                            }

                            if (isset($rule['maxAge'])){
                                $userMaxAge = $rule['maxAge'];
                            }

                            if (!self::checkIsDate($dataItem, $userMinAge, $userMaxAge)){
                                self::addInValidFieldToSession($key,'Not valid date specified');
                            }
                            else{
                                self::addValidField($key, $dataItem);
                            }
                            break;
                        case 'username':
                            if (!self::checkIsUsername($dataItem)){
                                self::addInValidFieldToSession($key, 'Field value must contain letters, numbers, -_\. and start with letter, from 5 to 30characters');
                            }
                            else{
                                self::addValidField($key, $dataItem);
                            }
                            break;
                        case 'password':
                            if (!self::checkIsPassword($dataItem)){
                                self::addInValidFieldToSession($key, 'Password field value must start from letter and can contain alphanumeric values, also  -_\.!@#$%^&*() these signs and space');
                            }
                            else{
                                self::addValidField($key, $dataItem);
                            }
                            break;
                        case 'email':
                            if (!self::checkIsEmail($dataItem)){
                                self::addInValidFieldToSession($key, 'The email you have specified is not valid');
                            }
                            else{
                                self::addValidField($key, $dataItem);
                            }
                            break;
                    }
                }

                if (isset($rule['length'])){
                    if (isset($rule['length']['min'])){
                        $minLength = $rule['length']['min'];
                    }
                    else{
                        $minLength = null;
                    }
                    if (isset($rule['length']['max'])){
                        $maxLength = $rule['length']['max'];
                    }
                    else{
                        $maxLength = null;
                    }
                    if (!self::checkLength($dataItem, $key, $minLength,$maxLength)){
                        if (!is_null($minLength) && !is_null($maxLength)){
                            $local_error_message = "Field value must be bigger than ".$minLength." and less than ".$maxLength;
                        }
                        else if (is_null($minLength)){
                            $local_error_message = "Field value must be less than ".$maxLength;
                        }
                        else if (is_null($maxLength)){
                            $local_error_message = "Field value must be bigger than ".$minLength;
                        }
                        self::addInValidFieldToSession($key, $local_error_message);
                    }
                    else{
                        self::addValidField($key, $dataItem);
                    }
                }

                //check number range
                if (isset($rule['type']) && $rule['type'] == 'number'){
                    if (isset($rule['min'])){
                        $validMaxLength = $validMinLength = false;
                        if ($dataItem <= $rule['min']){
                            self::addInValidFieldToSession($key, "Field value must be greater than ".$rule['min']);
                        }
                        else{
                            $validMinLength = true;
                        }
                    }

                    if (isset($rule['max'])){
                        if ($dataItem <= $rule['max']){
                            self::addInValidFieldToSession($key, "Field value must be less than ".$rule['max']);
                        }
                        else{
                            $validMaxLength = true;
                        }
                    }

                    if ($validMaxLength && $validMinLength){
                        self::addValidField($key, $dataItem);
                    }
                }

                if (isset($rule['in_array'])){
                    if (!in_array($dataItem, $rule['in_array'])){
                        self::addInValidFieldToSession($key, 'Not valid data specified');
                    }
                    else{
                        self::addValidField($key, $dataItem);
                    }
                }

                if (isset($rule['regexp'])){
                    if (!self::matchPattern($dataItem, $rule['regexp'])){
                        self::addInValidFieldToSession($key, 'Field is not valid');
                    }
                    else{
                        self::addValidField($key, $dataItem);
                    }
                }

            }
            else{
                return false;
            }
        }
        if (self::no_error()){
            return true;
        }
        else{
            return false;
        }
    }

    public static function hashPassword($password){
        return password_hash($password,PASSWORD_BCRYPT);
    }
	
	private static function checkEmpty($data){
        if ($data != ''){
            return true;
        }
        return false;
	}

    private static function checkIsNumber($data){
        if (is_numeric($data)){

            return true;
        }
        return false;
    }

    private static function checkIsAlpha($data){
        $splited_data = str_split($data);
        foreach ($splited_data as $item){
            if (!in_array(strtolower($item), self::$letters)){
                return false;
            }
        }
        return true;
    }

    private static function checkIsDate($data, $minage, $maxage){
        $date_error_msg = '';
        $date_from_str = DateTime::createFromFormat('Y-m-d', $data);
        if (!$date_from_str !== FALSE) {
            self::raiseError($date_error_msg);
            return false;
        }
        else{
            //logical check of date
            $now = new DateTime();
            $diff = $now->diff($date_from_str);
            $years = $diff->format("%y");
            if (!is_null($minage)){
                if ($years<$minage){
                    return false;
                }
            }

            if (!is_null($maxage)){
                if ($years>$maxage){
                    return false;
                }
            }
        }
        return true;
    }

    private static function checkIsUsername($data){
        if (!preg_match('/^[a-zA-Z][a-zA-Z0-9-_\.]{4,30}$/',$data)){
            return false;
        }
        return true;
    }

    private static function matchPattern($data, $regexp){
        if (!preg_match($regexp,$data)){
            return false;
        }
        return true;
    }

    private static function checkIsPassword($data){
        if (!preg_match('/^[a-zA-Z][a-zA-Z0-9-_\.!@#$%^&*(), ]{4,30}$/',$data)){
            return false;
        }
        return true;
    }

    private static function checkIsEmail($data){
        if (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    private static function checkLength($data, $key, $min = null,$max = null){
        if (is_numeric($data)){
            $data = intval($data);
            if (!is_null($min)){
                if ($data>$min){
                    $min_result = true;
                }
                else{
                    $min_result = false;
                }
            }
            else{
                $min_result = true;
            }

            if (!is_null($max)){
                if ($data<$max){
                    $max_result = true;
                }
                else{
                    $max_result = false;
                }
            }
            else{
                $max_result = true;
            }

            return ($min_result && $max_result);
        }
        else if (is_string($data)){
            $splited_data = str_split($data);
            $strlength = 0;
            foreach ($splited_data as $item){
                if (in_array(strtolower($item), self::$letters) || in_array($item, self::$characters) || in_array($item, self::$numbers)){
                    $strlength += 1;
                }
                else{
                    $strlength += 0.5;
                }
            }

            if (!is_null($strlength)){
                if ($strlength<$min){
                    return false;
                }
            }

            if (!is_null($max)){
                if ($strlength>$max){
                    return false;
                }
            }
            return true;
        }

    }

    private static function raiseError($error){
        SessionClass::set("error",$error);
    }

    private static function addValidField($key, $value){
        self::$fields_before_error[$key] = $value;
        SessionClass::set('fields_before_error',self::$fields_before_error);
    }

    private static function addInValidFieldToSession($key, $message){
        self::$error_fields[$key] = $message;
        SessionClass::set('error_fields', self::$error_fields);
    }

    private static function no_error(){
        if (empty(self::$error_fields)){
            return true;
        }
        return false;
    }



}
?>
