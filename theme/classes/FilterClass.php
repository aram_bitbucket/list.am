<?php
class FilterClass{
	
	private static $data;
	public static function filterFormData($data){
		self::$data = $data;
		self::trim();
		self::stripTags();
		self::escapeHtmlEntities();
		return self::$data;
	}

	public static function filterDbData($data){
		self::$data = $data;
		self::escapeHtmlEntities();
		return self::$data;
	}

	private static function trim(){
		if (!is_array(self::$data)){
			self::$data = trim(self::$data);
		}else{
			foreach (self::$data as &$dataIt){
				$dataIt = trim($dataIt);
			}
		}
	}

	private static function stripTags(){
		if (!is_array(self::$data)){
			self::$data = strip_tags(self::$data);
		}else{
			foreach (self::$data as &$dataIt){
				$dataIt = strip_tags($dataIt);
			}
		}
	}

	private static function escapeHtmlEntities(){
		if (!is_array(self::$data)){
			self::$data = htmlentities(self::$data, ENT_QUOTES);
		}else{
			foreach (self::$data as &$dataIt){
				$dataIt = htmlentities($dataIt, ENT_QUOTES);
			}
		}
	}
}