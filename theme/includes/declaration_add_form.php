<?php
//make empty unnecessary arrays
if (!isset($_POST['declaration_title'])){
    $declaration_title = $declaration_desc = $declaration_price = '';
    $declaration_title_error = $declaration_desc_error = $declaration_price_error = $image_error = '';
    $additional_field_error = $range_value = '';
    $error_fields = $fields_before_error = array();
}
?>
<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST"
      class="decalaration_form"
      id="add_decalaration_form" enctype="multipart/form-data">
    <div class="form-group">
        <label for="declaration_title">Հայտարարության վերնագիր</label>
        <input type="text" id="declaration_title" name="declaration_title" class="form-control"
               required="true" value="<?php echo $declaration_title; ?>"/>

        <p class="list_am_registration_form_error"><?php echo $declaration_title_error; ?></p>
    </div>
    <div class="form-group">
        <label for="declaration_desc">Հայտարարության նկարագարություն</label>
                            <textarea rows="10" cols="20" id="declaration_desc" name="declaration_desc"
                                      class="form-control" required><?php echo $declaration_desc; ?></textarea>

        <p class="list_am_registration_form_error"><?php echo $declaration_desc_error; ?></p>
    </div>
    <div class="form-group form-inline">
        <label for="declaration_price">Հայտարարության գին</label>
        <input type="number" id="declaration_price" name="declaration_price"
               class="form-control"
               required="true" value="<?php echo $declaration_price; ?>"/>
        <select name="price_unit" id="price_unit" class="form-control form-inline">
            <option value="դրամ">դրամ</option>
            <option value="դոլար">դոլար</option>
            <option value="ռուբլի">ռուբլի</option>
            <option value="եվրո">եվրո</option>
        </select>
    </div>
    <p class="list_am_registration_form_error"><?php echo $declaration_price_error; ?></p>
    <div class="form_group">
        <label for="upload_images">Հայտարարության նկարներ</label>

        <div id="upload_images" class="dropzone"></div>
        <p class="list_am_registration_form_error"
           id="image_error"><?php if ($image_error) echo $image_error; ?></p>
    </div>
    <?php
    if (isset($_SESSION['additional_form_field_values'])) {
        foreach ($_SESSION['additional_form_field_values'] as $field) {
            ?>
            <div class="form-group">
                <?php
                if ($field->show_field_name == "1") {
                    echo '<label for = "additional_field_' . $field->id . '">' . $field->field_name . '</label>';
                }
                switch ($field->field_type) {
                    case 'select':
                        echo '<select class = "form-control"  name = "additional_field_' . $field->id . '" id = "additional_field_' . $field->id . '">';
                        if (isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                            $selected = $fields_before_error["additional_field_" . $field->id];
                            echo '<option value = "' . $selected . '">' . $selected . '</option>';
                        }
                        foreach ($field->field_content as $field_content) {
                            if (isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                                //if there were fields before error
                                if ($selected != $field_content) {
                                    echo '<option value = "' . $field_content . '">' . $field_content . '</option>';
                                }
                            } else {
                                //if the form is new opened
                                echo '<option value = "' . $field_content . '">' . $field_content . '</option>';
                            }
                        }
                        echo '</select>';
                        break;
                    case 'radio':
                        if (isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                            echo '<label class="radio-inline">';
                            $selected = $fields_before_error["additional_field_" . $field->id];
                            echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $selected . '" checked = "true">';
                            echo $selected . '</label>';
                        }
                        for ($k = 0; $k < count($field->field_content); $k++) {
                            if (!isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                                echo '<label class="radio-inline">';
                                if ($k == 0) {
                                    echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $field->field_content[$k] . '" checked = "true">';
                                } else {
                                    echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $field->field_content[$k] . '">';
                                }
                                echo $field->field_content[$k] . '</label>';
                            } else if ($field->field_content[$k] != $selected) {
                                echo '<label class="radio-inline">';
                                echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $field->field_content[$k] . '">';
                                echo $field->field_content[$k] . '</label>';
                            }
                        }
                        break;
                    case 'textarea':
                        echo '<textarea class="form-control" rows="3"></textarea>';
                        break;
                    case 'range':
                        $range_value = isset($fields_before_error["additional_field_" . $field->id]) ? $fields_before_error["additional_field_" . $field->id] : "";
                        echo '<div class = "col-md-2 col-sm-3 col-xs-4" style="padding-left: 0;"><input type = "number" class="form-control" placeholder="from" name="additional_field_' . $field->id . '_1" id="additional_field_' . $field->id . '" value = "' . $range_value . '"></div>';
                        break;
                    case 'map':
                        $map_value = isset($fields_before_error["additional_field_" . $field->id]) ? $fields_before_error["additional_field_" . $field->id] : "";
                        ?>
                        <div style="width: 500px;height: 400px;background-color: #CCC;" class = "google_map_field"></div>
                            <input type="hidden" name = "additional_field_<?= $field->id; ?>"
                                   id = "map_selected_coords_"
                                   class = "map_selected_coords_area" value="<?= $map_value; ?>">
                        <?php
                        break;
                    case 'checkbox':
                        $field_value = isset($fields_before_error["additional_field_" . $field->id]) ? $fields_before_error["additional_field_" . $field->id] : "";
                        ?>
                        <input type="text" name = "additional_field_<?= $field->id; ?>" value="<?= $field_value; ?>">
                        <?php
                        break;
                }
                $additional_field_error = "";
                if ($error_fields) {
                    if (isset($error_fields['additional_field_' . $field->id])) {
                        $additional_field_error = $error_fields['additional_field_' . $field->id];
                    } else {
                        $needle = 'additional_field_' . $field->id . '_';
                        foreach ($error_fields as $err_field_key => $err_field) {
                            if (strpos($err_field_key, $needle) !== false) {
                                $additional_field_error = $err_field;
                                unset($error_fields[$err_field_key]);
                                break;
                            }
                        }
                    }
                }
                echo '<p class = "list_am_registration_form_error">' . $additional_field_error . '</p>';
                ?>
            </div>
            <?php
        }
        ?>
        <script class = "google_map_script">
            var markers = maps = map_coord_areas = map_center_coords = [];
            for (var i = 0;i<jQuery('.google_map_field').length;i++){
                console.log(i);
                jQuery('.google_map_field').eq(i).attr('id','map_'+i);
                jQuery('.map_selected_coords_area').eq(i).attr('id','map_selected_coords_'+i);
                markers.push(null);
                map_coord_areas.push(document.getElementById('map_selected_coords_'+i));
                map_center_coords.push({ lat: 40.1811100, lng: 44.5136100 });
                if (map_coord_areas[i].getAttribute('value') != ''){
                    var mapExistingCooords = mapCoordsCont.getAttribute('value').split(',');
                    map_center_coords[i] = { lat: parseFloat(mapExistingCooords[0]), lng: parseFloat(mapExistingCooords[1]) };
                }
            }

            function initialize() {
                var mapCanvas, mapOptions, map;
                for (var j = 0;j<jQuery('.google_map_field').length;j++){
                    mapCanvas = document.getElementById('map_'+j);
                    mapOptions = {
                        center: map_center_coords[j],
                        zoom: 10,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(mapCanvas, mapOptions);
                    maps.push(map);

                    // This event listener calls addMarker() when the map is clicked.
                    google.maps.event.addListener(map, 'click', function(event) {
                        addMarker(event.latLng, map, markers[j]);
                    });

                    // Add a marker at the center of the map.
                    addMarker(map_center_coords[j], map, markers[j]);
                }
            }

            // Adds a marker to the map.
            function addMarker(location, map, mapMarker) {
                // Add the marker at the clicked location
                if(mapMarker != null){
                    mapMarker.setMap(null);
                }
                // add marker
                mapMarker = new google.maps.Marker({
                    position: location,
                    map: map
                });

                if (location.L == undefined || location.H == undefined){
                    mapCoordsCont.setAttribute('value',mapCenterLatLng.lat + ',' + mapCenterLatLng.lng);
                }
                else{
                    mapCoordsCont.setAttribute('value',location.H + ',' + location.L);
                }
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        <?php
        echo '<button type="submit" name = "submit_declaration" id = "submit_declaration" class="btn btn-success col-lg-2" style = "float:right;">Submit</button></form>';
    }
    ?>
</form>