<?php
//edit form;
$user_id = define_user();
global $wpdb;
$post_data = $wpdb->get_row("SELECT declaration_title, declaration_desc, price, declaration_images, additional_fields, post_type FROM list_am_declarations WHERE post_id = '" . $_GET['post_id'] . "' AND user_id = '" . $user_id . "'");
if (!empty($post_data)) {
    if (isset($post_data->price)) {
        $prices = array(
            'դրամ' => 'դրամ',
            'դոլար' => 'դոլար',
            'ռուբլի' => 'ռուբլի',
            'եվրո' => 'եվրո'
        );

        $price_parts = explode(" ", $post_data->price);
        $price_number = $price_parts[0];
        $price_unit = $price_parts[1];
    }

    $existing_declaration_images = $post_data->declaration_images;
    SessionClass::set('existing_declaration_images', json_decode($existing_declaration_images));
    $additional_fields_selected = json_decode($post_data->additional_fields);
    $all_additional_fields_for_edit = get_additional_fields(ucfirst($post_data->post_type));

    //get url post_id
    $server_reuqest_uri = $_SERVER['REQUEST_URI'];
    $server_reuqest_uri = parse_url($server_reuqest_uri);
    parse_str($server_reuqest_uri['query'], $server_reuqest_uri);
    SessionClass::set('real_post_id', $server_reuqest_uri['post_id']);



    //make empty unnecessary arrays
    if (!isset($_POST['declaration_title'])){
        $declaration_title_error = $declaration_desc_error = $declaration_price_error = '';
        $additional_field_error = '';
        $error_fields = $fields_before_error = array();
    }
    ?>
    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST"
          class="decalaration_form" id="edit_decalaration_form" enctype="multipart/form-data"
          name="decalaration_form">
        <div class="form-group">
            <label for="declaration_title">Հայտարարության վերնագիր</label>
            <input type="text" id="declaration_title" name="declaration_title"
                   class="form-control"
                   value="<?php if (isset($post_data->declaration_title)) echo $post_data->declaration_title; ?>"
                   required="true"/>
            <p class="list_am_registration_form_error"><?php echo $declaration_title_error; ?></p>
        </div>
        <div class="form-group">
            <label for="declaration_desc">Հայտարարության նկարագարություն</label>
                                    <textarea rows="10" cols="20" id="declaration_desc" name="declaration_desc"
                                              class="form-control"
                                              required><?php if (isset($post_data->declaration_desc)) echo $post_data->declaration_desc; ?></textarea>
            <p class="list_am_registration_form_error"><?php echo $declaration_desc_error; ?></p>
        </div>
        <div class="form-group form-inline">
            <label for="declaration_price">Հայտարարության գին</label>
            <input type="number" id="declaration_price" name="declaration_price"
                   class="form-control" required="true"
                   value="<?php if (isset($price_number)) echo $price_number; ?>"/>
            <select name="price_unit" id="price_unit" class="form-control form-inline">
                <option
                    value="<?php if (isset($price_unit)) echo $price_unit; ?>"><?php if (isset($price_unit)) echo $prices[$price_unit]; ?></option>
                <?php
                foreach ($prices as $price => $price_upper) {
                    if ($price != $price_unit) {
                        ?>
                        <option value="<?= $price; ?>"><?= $price_upper; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
            <p class="list_am_registration_form_error"><?php echo $declaration_price_error; ?></p>
        </div>
        <div class="form_group">
            <label for="upload_images">Հայտարարության նկարներ</label>

            <div id="already_uploaded_images" style="border: 1px solid #005082;">
                <?php
                if (isset($existing_declaration_images)) {
                    $existing_declaration_images = json_decode($existing_declaration_images);
                    $first_img = reset($existing_declaration_images);
                    foreach ($existing_declaration_images as $img_name => $img_src) {
                        ?>
                        <div class="uploaded_img_content">
                            <img src="<?php echo $img_src ?>" alt="<?php echo $img_name ?>"
                                 width="200">
                            <?php if ($first_img != $img_src) { ?>
                                <a href="javascript:delete_post_image('<?php echo $img_src ?>');">Delete
                                    Image</a>
                            <?php } ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div id="upload_images" class="dropzone"></div>
        </div>
        <?php
        if (isset($all_additional_fields_for_edit)) {
            foreach ($all_additional_fields_for_edit as $field) {
                ?>
                <div class="form-group">
                    <?php
                    $field_name = $field->field_name;
                    if ($field->show_field_name == "1") {
                        echo '<label for = "additional_field_' . $field->id . '">' . $field_name . '</label>';
                    }
                    switch ($field->field_type) {
                        case 'select':
                            echo '<select class = "form-control"  name = "additional_field_' . $field->id . '" id = "additional_field_' . $field->id . '">';
                            if (isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                                $selected = $fields_before_error["additional_field_" . $field->id];
                                echo '<option value = "' . $selected . '">' . $selected . '</option>';
                            }
                            foreach ($field->field_content as $field_content) {
                                if (isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                                    if ($selected != $field_content) {
                                        echo '<option value = "' . $field_content . '">' . $field_content . '</option>';
                                    }
                                } else {
                                    //if edit form is new opened
                                    if ($additional_fields_selected->$field_name == $field_content || (is_null($additional_fields_selected->$field_name) && $field_content == 'Any'))
                                        echo '<option value = "' . $field_content . '" selected>' . $field_content . '</option>';
                                    else
                                        echo '<option value = "' . $field_content . '">' . $field_content . '</option>';
                                }
                            }
                            echo '</select>';
                            break;
                        case 'radio':
                            if (isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                                echo '<label class="radio-inline">';
                                $selected = $fields_before_error["additional_field_" . $field->id];
                                echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $selected . '" checked = "true">';
                                echo $selected . '</label>';
                            }
                            for ($k = 0; $k < count($field->field_content); $k++) {
                                if (!isset($fields_before_error, $fields_before_error["additional_field_" . $field->id])) {
                                    echo '<label class="radio-inline">';
                                    if ($additional_fields_selected->$field_name == $field->field_content[$k] || (is_null($additional_fields_selected->$field_name) && $field->field_content[$k] == 'None')) {
                                        echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $field->field_content[$k] . '" checked = "true">';
                                    } else {
                                        echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $field->field_content[$k] . '">';
                                    }
                                    echo $field->field_content[$k] . '</label>';
                                } else if ($field->field_content[$k] != $selected) {
                                    echo '<label class="radio-inline">';
                                    echo '<input type="radio" name="additional_field_' . $field->id . '" id="additional_field_' . $field->id . '" value="' . $field->field_content[$k] . '">';
                                    echo $field->field_content[$k] . '</label>';
                                }
                            }
                            break;
                        case 'textarea':
                            echo '<textarea class="form-control" rows="3"></textarea>';
                            break;
                        case 'range':
                            $range_value = $additional_fields_selected->$field_name;
                            if (isset($fields_before_error["additional_field_" . $field->id])){
                                $range_value =  $fields_before_error["additional_field_" . $field->id];
                            }

                            echo '<div class = "col-md-2 col-sm-3 col-xs-4" style="padding-left: 0;"><input type = "number" class="form-control" placeholder="from" name="additional_field_' . $field->id . '_1" id="additional_field_' . $field->id . '" value = "' . $range_value . '"></div>';
                            break;
                        case 'map':
                            $map_coords = $additional_fields_selected->$field_name;
                            if (isset($fields_before_error["additional_field_" . $field->id])){
                               $map_coords =  $fields_before_error["additional_field_" . $field->id];
                            }
                            ?>
                            <div id="map_<?= $field->id; ?>" style="width: 500px;height: 400px;background-color: #CCC;"></div>
                            <input type="hidden" name = "additional_field_<?= $field->id; ?>"
                                   id = "map_selected_coords_<?= $field->id; ?>" value = "<?= $map_coords; ?>">
                            <script>
                                var mapMarker = null;
                                var map_id = '<?php echo $field->id; ?>';
                                var mapCoordsCont = document.getElementById('map_selected_coords_'+map_id);
                                var mapExistingCooords = mapCoordsCont.getAttribute('value').split(',');
                                var mapCenterLatLng = { lat: parseFloat(mapExistingCooords[0]), lng: parseFloat(mapExistingCooords[1]) };
                                function initialize() {
                                    var mapCanvas = document.getElementById('map_'+map_id);
                                    var mapOptions = {
                                        center: mapCenterLatLng,
                                        zoom: 12,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    };

                                    var map = new google.maps.Map(mapCanvas, mapOptions);

                                    // This event listener calls addMarker() when the map is clicked.
                                    google.maps.event.addListener(map, 'click', function(event) {
                                        addMarker(event.latLng, map);
                                    });

                                    // Add a marker at the center of the map.
                                    addMarker(mapCenterLatLng, map);
                                }

                                // Adds a marker to the map.
                                function addMarker(location, map) {
                                    // Add the marker at the clicked location, and add the next-available label
                                    // from the array of alphabetical characters.
                                    if(mapMarker != null){
                                        mapMarker.setMap(null);
                                    }
                                    // add marker
                                    mapMarker = new google.maps.Marker({
                                        position: location,
                                        map: map
                                    });

                                    if (location.L == undefined || location.H == undefined){
                                        mapCoordsCont.setAttribute('value',mapCenterLatLng.lat + ',' + mapCenterLatLng.lng);
                                    }
                                    else{
                                        mapCoordsCont.setAttribute('value',location.H + ',' + location.L)
                                    }

                                }

                                google.maps.event.addDomListener(window, 'load', initialize);


                            </script>
                            <?php
                            break;
                    }
                    $additional_field_error = "";
                    if ($error_fields) {
                        if (isset($error_fields['additional_field_' . $field->id])) {
                            $additional_field_error = $error_fields['additional_field_' . $field->id];
                        } else {
                            $needle = 'additional_field_' . $field->id . '_';
                            foreach ($error_fields as $err_field_key => $err_field) {
                                if (strpos($err_field_key, $needle) !== false) {
                                    $additional_field_error = $err_field;
                                    unset($error_fields[$err_field_key]);
                                    break;
                                }
                            }
                        }
                    }
                    echo '<p class = "list_am_registration_form_error">' . $additional_field_error . '</p>';
                    ?>
                </div>
                <?php
            }
        }
        ?>
        <button type="submit" name="submit_declaration" id="add_declaration"
                class="btn btn-success col-lg-2" style="float:right;">Submit
        </button>
    </form>
    <?php
} else {
    echo 'There is no content with such id';
}
?>