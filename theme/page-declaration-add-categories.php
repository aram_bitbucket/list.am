<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<script type="text/javascript">
    var site_url = '<?= get_site_url(); ?>';
    if (localStorage.getItem('site_url') == null) {
        localStorage.setItem("site_url", site_url);
    }
</script>
<?php
get_header(); ?>
<div id="primary" class="content-area" xmlns="http://www.w3.org/1999/html">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                // Post thumbnail.
                twentyfifteen_post_thumbnail();
                ?>
                <?php
                if (isset($_GET['post_id'])) {
                    global $wpdb;
                    $prices = array('????','?????','????','??????');
                    $post_data = $wpdb->get_row("SELECT declaration_title, declaration_desc, price, declaration_images, additional_fields FROM list_am_declarations WHERE post_id = '" . $_GET['post_id'] . "'");
                    if (isset($post_data->price)){
                        $price_parts = explode(" ", $post_data->price);
                        $price_number = $price_parts[0];
                        $price_unit = $price_parts[1];
                    }
                    ?>
                    <style>
                        .declaration_categories{
                            display:none;
                        }

                        .entry-header{
                            display: none;
                        }
                    </style>
                    <div class="entry-content">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST"
                              class="decalaration_form" enctype="multipart/form-data" name="decalaration_form">
                            <div class="form-group">
                                <label for="declaration_title">?????????????? ????????</label>
                                <input type="text" id="declaration_title" name="declaration_title" class="form-control"
                                       required="true" value = "<?php if (isset($post_data->declaration_title)) echo $post_data->declaration_title; ?>"/>
                            </div>
                            <div class="form-group">
                                <label for="declaration_desc">?????????????? ??????????????</label>
            <textarea rows="10" cols="20" id="declaration_desc" name="declaration_desc"
                      class="form-control" required >
            <?php if (isset($post_data->declaration_desc)) echo $post_data->declaration_desc; ?></textarea>
                            </div>
                            <div class="form-group form-inline">
                                <label for="declaration_price">?????????????? ???</label>
                                <input type="number" id="declaration_price" name="declaration_price" class="form-control"
                                       required="true" value = "<?php if (isset($price_number)) echo $price_number; ?>"/>
                                <select name="price_unit" id="price_unit" class="form-control form-inline">
                                    <option value="<?php if (isset($price_unit)) echo $price_unit; ?>"><?php if (isset($price_unit)) echo ucfirst($price_unit); ?></option>
                                    <?php
                                    foreach ($prices as $price){
                                        if ($price != $price_unit){
                                            ?>
                                            <option value="<?= $price; ?>"><?= ucfirst($price); ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form_group">
                                <label for="upload_images">?????????????? ????(???)</label>
                                <div id="upload_images" class="dropzone"></div>
                            </div>
                        </form>
                    </div>
                    <?php
                }
                ?>
                <div class="entry-header">
                    <p style="font-size: 19px;font-weight: bold">Please choose a category</p>
                </div>
                <div class="entry-content">
                    <div class="declaration_categories">
                        <ul>
                            <?php
                            $args = array(
                                'type' => 'post',
                                'hide_empty' => 0,
                                'exclude' => get_cat_ID('Uncategorized')
                            );
                            wp_list_categories(apply_filters('widget_categories_args', $args));
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- .entry-content -->
            </article><!-- #post-## -->
            <?php

            // End the loop.
        endwhile;
        ?>

    </main>
    <!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
