<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) :
        the_post();

        // Include the page content template.
        ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php
            // Post thumbnail.
            twentyfifteen_post_thumbnail();
            ?>

            <div class="entry-content">
                <?php
                if (!isset($_SESSION['fb_user_id'])) {
                    $current_user = wp_get_current_user();
                } else {
                    //user is logged in via fb
                    global $wpdb;
                    $row = $wpdb->get_row("SELECT user_id FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = '" . $_SESSION['fb_user_id'] . "'");
                    $current_user = get_user_by('id', $row->user_id);
                }
                $user_display_name_parts = explode(" ", $current_user->display_name);
                $firstname = $user_display_name_parts[0];
                if (isset($user_display_name_parts[1])) {
                    if (empty($user_display_name_parts[1])) {
                        $lastname = $user_display_name_parts[2];
                    } else {
                        $lastname = $user_display_name_parts[1];
                    }

                } else {
                    $lastname = $firstname;
                }
                $email = $current_user->user_email;
                $birthday = get_user_meta($current_user->ID, 'user_birthday', true);
                $mobile_phone = get_user_meta($current_user->ID, 'phone_number', true);

                ?>
                <form action="" method="POST">
                    <span><b>Fisrtname: </b><?php echo $firstname; ?></span>
                    <br><br>
                    <span><b>Lastname: </b><?php echo $lastname; ?></span>
                    <br><br>
                    <span><b>Email: </b><?php echo $email; ?></span>
                    <br><br>
                    <span><b>Birthday: </b><?php echo $birthday; ?></span>
                    <br><br>
                    <?php
                    if (!empty($mobile_phone)) {
                        ?>
                        <span><b>Mobile Phone: </b><?php echo $mobile_phone; ?></span>
                        <?php
                    }
                    ?>
                </form>
                <?php
                global $wpdb;
                if (isset($_SESSION['fb_user_id'])) {
                    $user_obj = $wpdb->get_row("SELECT user_id FROM " . $wpdb->usermeta . " WHERE meta_key = 'user_fb_id' AND meta_value = " . $_SESSION['fb_user_id']);
                    $user_id = $user_obj->user_id;

                } else {
                    $user_id = get_current_user_id();
                }

                $account_merge = get_user_meta($user_id, 'merged_accounts', true);
                if (empty($account_merge)){
                    //user hasnot merged account with another user
                    $_SESSION['want_to_merge_accounts'] = $user_id;
                    if (isset($_SESSION['fb_user_id'])) {
                        echo '<h3>Do you want to log in with your Wordpress account also and merge profiles</h3>';
                        wp_login_form(
                            array(
                                'redirect' => home_url(),
                                'remember' => false
                            )
                        );
                    }
                    else{
                        echo '<h3>Do you want to log in with your Facebook account also and merge profiles</h3>';
                        ?>
                        <fb:login-button size="xlarge" scope="public_profile,email,user_birthday"
                                         onlogin="checkLoginState();">Log in
                        </fb:login-button>
                        <div id="status"></div>
                        <?php
                    }
                }
                ?>
            </div>
        </article>

        <?php
        endwhile;
        ?>
    </main>
</div><!-- .content-area -->

<?php get_footer(); ?>
