var myDropzone;
function delete_post_image(img){
    jQuery('img[src="'+img+'"]').parent().remove();
    var wp_admin_ajax_url = localStorage.getItem('site_url') + '/wp-admin/admin-ajax.php';
    ++myDropzone.options.maxFiles;
    jQuery.ajax({
        url: wp_admin_ajax_url,
        method: "POST",
        data: {
            "action": 'delete_post_image',
            "image": img
        }
    }).success(function (data) {
        console.log('Post image deleted');
    });
}

jQuery(document).ready(function () {

    //spinner options spin.js
    var opts = {
        lines: 13 // The number of lines to draw
        , length: 28 // The length of each line
        , width: 14 // The line thickness
        , radius: 42 // The radius of the inner circle
        , scale: 1 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#000' // #rgb or #rrggbb or array of colors
        , opacity: 0.25 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 60 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '50%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
    };
//making spinner object
    var spinner = new Spinner(opts);

//dropzone for uploading images
    Dropzone.autoDiscover = false;

    myDropzone = new Dropzone("#upload_images", {
        url: localStorage.getItem('site_url') + '/declaration-add-form/',
        paramName: "declaration_images", // The name that will be used to transfer the file
        parallelUploads: 10,
        maxFilesize: 5,
        uploadMultiple: true,// MB
        addRemoveLinks: true,
        maxFiles: 10 - jQuery('.uploaded_img_content').length,
        acceptedFiles: 'image/*',
        autoProcessQueue: false,
        error: function (file, errorMessage) {
            alert(errorMessage);
            myDropzone.removeFile(file);
        },
        sendingmultiple: function (file, xhr, formData) {
            spinner.spin();
            document.getElementsByClassName('decalaration_form')[0].appendChild(spinner.el);
            jQuery('.declaration_add_form_spinner').show();
        },
        successmultiple: function () {
            console.log('Successful uploading');
        },
        completemultiple: function () {
            console.log('Uploading completed');
            spinner.stop();
            jQuery('.declaration_add_form_spinner').hide();
        },
        queuecomplete: function () {
            console.log('queuecomplete');
            jQuery(".decalaration_form").unbind('submit').submit();
        }
    });

    jQuery('#add_decalaration_form').submit(function () {
        if (myDropzone.getQueuedFiles().length>0){
            myDropzone.processQueue();
        }
        else{
            jQuery('#image_error').text('You must upload at least 1 image');
        }
        return false;
    });

    jQuery('#edit_decalaration_form').submit(function () {
        if (myDropzone.getQueuedFiles().length>0){
            myDropzone.processQueue();
        }
    });
});
