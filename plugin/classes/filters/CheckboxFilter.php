<?php
require_once('MultipleselectFilter.php');
class CheckboxFilter extends MultipleselectFilter{

    public function __construct($args){
        parent::__construct($args);
    }

    public function getView(){
        $view = '';
        if (empty($this->unique_name)  || empty($this->items) || !is_array($this->items)){
            return "";
        }

        foreach ($this->items as $cont_item){
            $view .= '<input type="checkbox" name="'.$this->unique_name.'[]" value = "'.$cont_item.'">'.$cont_item.'<br>';
        }
        $view.='<input type = "submit" name = "'.$this->unique_name.'" id = "'.$this->unique_name.'_submit" placeholder = "to" onclick = "filter_form.submit();">';
        return $view;

    }

}