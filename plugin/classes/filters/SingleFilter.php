<?php
require_once('AbstractFilter.php');
class SingleFilter extends AbstractFilter{
    protected $unique_name,$items, $sel_v;
    protected function __construct($args){
        if (isset($args['field_name'])){
            $this->unique_name = $args['field_name'];
        }

        if (isset($args['content'])){
            $this->items = $args['content'];
        }

        if (isset($args['sel_v'])){
            $this->sel_v = $args['sel_v'];
        }
    }

    public function getQuery(){
        if(empty($this->unique_name) || empty($this->sel_v)){
            return "";
        }
        $query = "(field_name='".$this->unique_name."' AND selected_value = '".$this->sel_v."')";
        return $query;
    }
}