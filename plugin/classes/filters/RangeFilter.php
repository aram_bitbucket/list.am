<?php
require_once('AbstractFilter.php');
class RangeFilter extends AbstractFilter{

    private $unique_name, $sel_v;
    public function __construct($args){
        if (isset($args['field_name'])){
            $this->unique_name = $args['field_name'];
        }

        if (isset($args['sel_v'])){
            $this->sel_v = $args['sel_v'];
        }
    }

    public function getView(){
        if (empty($this->unique_name)){
            return "";
        }

        $from_value = $to_value = '';
        if (isset($_GET[$this->unique_name.'_from']) && !empty($_GET[$this->unique_name.'_from'])){
            $from_value = $_GET[$this->unique_name.'_from'];
        }
        if (isset($_GET[$this->unique_name.'_to']) && !empty($_GET[$this->unique_name.'_to'])){
            $to_value = $_GET[$this->unique_name.'_to'];
        }
        $view = '<input type = "number" name = "'.$this->unique_name.'_from" id = "'.$this->unique_name.'_from" placeholder = "from" value = "'.$from_value.'">';
        $view.= '<input type = "number" name = "'.$this->unique_name.'_to" id = "'.$this->unique_name.'_to" placeholder = "to" value = "'.$to_value.'">';
        $view.= '<input type = "submit" name = "'.$this->unique_name.'" id = "'.$this->unique_name.'_submit" placeholder = "to" onclick = "filter_form.submit();">';
        return $view;
    }

    public function getQuery(){
        if (empty($this->unique_name) || empty($this->sel_v)){
            return "";
        }

        if (!is_array($this->sel_v) || count($this->sel_v)!=2){
            return "";
        }

        $query = "(field_name='".$this->unique_name."' AND selected_value >= ".$this->sel_v[0]." AND selected_value <= ".$this->sel_v[1].")";
        return $query;
    }
}