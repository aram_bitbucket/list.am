<?php
require_once('SingleFilter.php');
class SelectFilter extends SingleFilter{

    public function __construct($args){
        parent::__construct($args);
    }

    public function getView(){
        if (empty($this->items) || empty($this->unique_name)){
            return "";
        }

        if (!is_array($this->items)){
            return "";
        }

        $view = '';
        $view.='<select name = "'.$this->unique_name.'" id = "'.$this->unique_name.'" onchange = "filter_form.submit();">';
        $view.='<option value = "">Select value</option>';
        foreach ($this->items as $item_text){
            $selected_or_not = '';
            if (isset($_GET[$this->unique_name]) && $_GET[$this->unique_name] == $item_text){
                $selected_or_not = 'selected';
            }
            if ($item_text != 'Any'){
                $view.='<option value = "'.$item_text.'" '.$selected_or_not.'>'.$item_text.'</option>';
            }
        }
        $view.='</select>';
        return $view;
    }
}