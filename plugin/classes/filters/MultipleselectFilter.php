<?php
require_once('AbstractFilter.php');
class MultipleselectFilter extends AbstractFilter
{
    protected function __construct($args){
        if (isset($args['field_name'])){
            $this->unique_name = $args['field_name'];
        }

        if (isset($args['content'])){
            $this->items = $args['content'];
        }

        if (isset($args['sel_v'])){
            $this->sel_v = $args['sel_v'];
        }
    }

    public function getQuery(){
        if (empty($this->items) || empty($this->unique_name)){
            return "";
        }

        if (!is_array($this->items) || count($this->items)<2){
            return "";
        }

        $content = implode(',', $this->items);
        $query = "(field_name = '" . $this->unique_name . "' AND selected_value IN(".$content."))";
        return $query;
    }
}