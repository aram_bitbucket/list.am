<?php
require_once('SingleFilter.php');
class RadioFilter extends SingleFilter{

    public function __construct($args){
        parent::__construct($args);
    }

    public function getView(){
        if (empty($this->items) || empty($this->unique_name)){
            return "";
        }

        if (!is_array($this->items)){
            return "";
        }

        $view = '';
        $field_index = 0;
        foreach ($this->items as $item){
            if ($item != 'None'){
                ++$field_index;
                $item_id = $this->unique_name .'_'. $field_index;
                $view.='<label for = "' . $item_id . '" class = "list_am_filter_radio">';
                $checked_or_not = '';
                if (isset($_GET[$this->unique_name]) && $_GET[$this->unique_name] == $item){
                    $checked_or_not = 'checked';
                }
                $view.='<input type="radio" name="'.$this->unique_name.'" id = "'.$item_id.'" onclick = "filter_form.submit();" value = "'.$item.'" '.$checked_or_not.'>'.$item.'</label><br>';
            }
        }
        return $view;
    }
}