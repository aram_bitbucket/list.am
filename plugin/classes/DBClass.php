<?php
class DBClass
{
    private $db;

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    public function save_post_type_data($post_type_data)
    {
        $table = 'list_am_post_types';
        if (empty($post_type_data['id'])) {
            $this->db->insert($table, $post_type_data);
        } else {
            $post_type_id = intval($post_type_data['id']);
            unset($post_type_data['id']);
            $this->db->update($table, $post_type_data, array('id' => $post_type_id));
        }
    }

    public function save_post_type_additional_fields($post_type_field_data)
    {
        $table = 'list_am_post_type_fields';
        if (empty($post_type_field_data['id'])) {
            $this->db->insert($table, $post_type_field_data);
        } else {
            $post_type_field_id = intval($post_type_field_data['id']);
            unset($post_type_field_data['id']);
            $this->db->update($table, $post_type_field_data, array('id' => $post_type_field_id));
        }
    }

    public function save_list_am_filter($filter_data)
    {
        $table = 'list_am_filters';
        $filter_id = isset($filter_data['id'])?$filter_data['id']:null;
        if (is_null($filter_id)) {
            $this->db->insert($table, $filter_data);
        } else {
            unset($filter_data['id']);
            $this->db->update($table, $filter_data, array('id' => $filter_id));
        }
    }

    public function createTable($sql)
    {
        $charset_collate = $this->db->get_charset_collate();
        $sql_and_charset = $sql . " " . $charset_collate;
        dbDelta($sql_and_charset);
    }

    public function dropTable($table)
    {
        $this->db->query("DROP TABLE IF EXISTS " . $table);
    }

    public function findAll($table)
    {
        return $this->db->get_results('SELECT * FROM ' . $table);
    }

    public function get_post_type_label_name($post_type_id)
    {
        $res = $this->db->get_row("SELECT label,name FROM list_am_post_types WHERE id = '" . $post_type_id . "' LIMIT 1");
        return $res;
    }

    public function get_filter_label_by_name($filter_name){
        return $this->db->get_var("SELECT label FROM list_am_filters WHERE name = '".$filter_name."' LIMIT 1");
    }

    public function get_post_type_label_by_name($post_type_name){
        return $this->db->get_var("SELECT label FROM list_am_post_types WHERE name = '".$post_type_name."' LIMIT 1");
    }

    public function get_filters_for_dropdown(){
        return $this->db->get_results('SELECT name, label FROM list_am_filters');
    }

    public function delete_by_id($table, $id)
    {
        $this->db->delete($table, array('id' => $id));
    }

    public function delete_post_type_fields($post_type_name)
    {
        $this->db->delete('list_am_post_type_fields', array('post_type_name' => $post_type_name));
    }
}