<?php

class List_am_category_widget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array('classname' => 'list_am_widget_categories', 'description' => __("A list of List.am categories."));
        parent::__construct('list_am_categories', __('List.am Categories'), $widget_ops);
    }

    public function widget($args, $instance)
    {
        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? __('Categories') : $instance['title'], $instance, $this->id_base);

        echo $args['before_widget'];
        if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        $cat_args = array(
            'orderby' => 'name',
            'hide_empty' => 0,
            'exclude' => '1'
        );

        ?>
        <ul>
            <?php
            $cat_args['title_li'] = '';

            /**
             * Filter the arguments for the Categories widget.
             *
             * @since 2.8.0
             *
             * @param array $cat_args An array of Categories widget options.
             */
            wp_list_categories(apply_filters('widget_categories_args', $cat_args));
            ?>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    public function form($instance)
    {
        //Defaults
        $instance = wp_parse_args((array)$instance, array('title' => ''));
        $title = esc_attr($instance['title']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>
        <?php
    }
}