<?php

class List_am_filters_widget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array('classname' => 'list_am_filters_widget', 'description' => __("List.am custom category filters"));
        parent::__construct('list_am_category_filters', __('List.am Category Filters'), $widget_ops);
    }

    public function widget($args, $instance)
    {
        global $post;
        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? __('Categories') : $instance['title'], $instance, $this->id_base);

        echo $args['before_widget'];
        if (is_category() || is_search()) {

            $post_category = wp_get_post_categories($post->ID);
            $post_cat_name = get_cat_name($post_category[0]);

            if (is_category_list_am_post_type($post_cat_name)) {
                if ($title) {
                    echo $args['before_title'] . $title . $args['after_title'];
                }

                $category_custom_fields = get_category_custom_fields($post_cat_name);
                ?>
                <ul>
                    <?php
                    echo '<form method="GET" action="" name = "filter_form">';
                    $price_obj = new RangeFilter(array('field_name'=>'price'));
                    echo '<h3>Price</h3>';
                    echo '<div data-type = "range">';
                    echo $price_obj->getView();
                    echo '</div>';
                    foreach ($category_custom_fields as $category_custom_field) {
                        $filterClassName = ucfirst($category_custom_field->filter_name).'Filter';
                        $additional_form_field_content = explode(",", $category_custom_field->field_content);
                        if (class_exists($filterClassName)){
                            if ($category_custom_field->show_field_name == 1) {
                                echo '<h3>' . $category_custom_field->label . '</h3>';
                            }
                            $filterObj = new $filterClassName(
                                array(
                                    'content'=>$additional_form_field_content,
                                    'field_name'=>$category_custom_field->field_name
                                )
                            );
                            echo '<div data-type = "'.$category_custom_field->filter_name.'">';
                            echo $filterObj->getView();
                            echo '</div>';
                        }
                    }
                    echo '</form>';
                    ?>
                </ul>
                <?php
                echo $args['after_widget'];
            }
        }
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    public function form($instance)
    {
        //Defaults
        $instance = wp_parse_args((array)$instance, array('title' => ''));
        $title = esc_attr($instance['title']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>
        <?php
    }
}