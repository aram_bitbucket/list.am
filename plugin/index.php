<?php

/**
 * Plugin Name: Custom List.am
 * Version: 1.0
 * Author: Hrayr Hovakimyan
 */

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
require('classes/DBClass.php');
require('classes/widgets/List_am_category.php');
require('classes/widgets/List_am_filters.php');
require('classes/filters/RadioFilter.php');
require('classes/filters/RangeFilter.php');
require('classes/filters/SelectFilter.php');
require('classes/filters/CheckboxFilter.php');
$custom_post_type_menu_positions = array(
    '5' => '5 - below Posts',
    '10' => '10 - below Media',
    '20' => '20 - below Pages',
    '25' => '25 - below Comments',
    '60' => '60 - below first separator',
    '65' => '65 - below Plugins',
    '70' => '70 - below Users',
    '75' => '75 - below Tools',
    '80' => '80 - below Settings',
    '100' => '100 - below second separator'
);

function is_category_list_am_post_type($cat_name)
{
    global $wpdb;
    $custom_post_type = $wpdb->get_row("SELECT label FROM list_am_post_types WHERE name = '" . $cat_name . "' LIMIT 1");
    if (!is_null($custom_post_type)) {
        return true;
    }
    return false;
}

function get_category_custom_fields($cat_name)
{
    global $wpdb;
    $category_filters = $wpdb->get_results("SELECT * FROM list_am_post_type_fields WHERE post_type_name = '" . $cat_name . "'");
    return $category_filters;
}

function load_listam_admin_styles_and_scripts()
{
    wp_enqueue_style('listam_admin_style', plugin_dir_url(__FILE__) . 'adminStyle.css');
    wp_enqueue_script('listam_admin_script', plugin_dir_url(__FILE__) . 'adminScript.js');
}

class ListAm_post_type
{
    private $dbObject;
    private $_default_args = array(
        'public' => true,
        'has_archive' => false,
        'supports' => array('title', 'editor'),
        'taxonomies' => array('category'),
        'capability_type' => 'post',
        'delete_with_user' => true,
        'exclude_from_search' => false
    );

    public function __construct()
    {
        $this->dbObject = new DBClass();
    }

    public function on_plugin_activation()
    {
        //configure permalinks
        $this->register_custom_post_type();
        flush_rewrite_rules();

        //auto create needed tables
        $post_type_table_sql = "CREATE TABLE list_am_post_types (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(200) NOT NULL,
  label varchar(200) NOT NULL,
  description text  NOT NULL,
  menu_position int(11) NOT NULL,
  menu_icon varchar(150) NOT NULL,
  parent_category varchar(200) NOT NULL,
  UNIQUE KEY id (id)
)";

        $post_type_field_table_sql = "CREATE TABLE list_am_post_type_fields (
  id int(11) NOT NULL AUTO_INCREMENT,
  field_name varchar(200) NOT NULL,
  label varchar(200) NOT NULL,
  field_type varchar(200)  NOT NULL,
  field_content text NOT NULL,
  post_type_name varchar(200) NOT NULL,
  show_field_name boolean NOT NULL,
  UNIQUE KEY id (id)
)";

        $declaration_fields_sql = "CREATE TABLE list_am_declarations (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int (11) NOT NULL,
  post_id int(11) NOT NULL,
  post_type varchar(200) NOT NULL,
  declaration_title varchar(200) NOT NULL,
  declaration_desc text NOT NULL,
  price varchar(200) NOT NULL,
  declaration_images text NOT NULL,
  UNIQUE KEY id (id)
)";

        $declaration_additional_fields_sql = "CREATE TABLE list_am_post_additional_fields (
  id int(11) NOT NULL AUTO_INCREMENT,
  post_id int (11) NOT NULL,
  field_name varchar(200) NOT NULL,
  selected_value varchar(200) NOT NULL,
  UNIQUE KEY id (id)
)";

        $list_am_filters_sql = "CREATE TABLE list_am_filters (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(200) NOT NULL,
  label varchar(200) NOT NULL,
  UNIQUE KEY id (id)
)";

        $this->dbObject->createTable($post_type_table_sql);
        $this->dbObject->createTable($post_type_field_table_sql);
        $this->dbObject->createTable($declaration_fields_sql);
        $this->dbObject->createTable($declaration_additional_fields_sql);
        $this->dbObject->createTable($list_am_filters_sql);

        //make wordpress upload directory
        $wp_upload_dir = wp_upload_dir();
        if (!file_exists($wp_upload_dir['path'] . '/list_am')) {
            mkdir($wp_upload_dir['path'] . '/list_am');
        }


        //auto create wordpress pages
        $auto_creation_pages = array(
            array(
                'title' => 'Declaration add form',
                'slug' => 'declaration-add-form'
            ),
            array(
                'title' => 'List.am Registration',
                'slug' => 'list-am-registration'
            ),
            array(
                'title' => 'List am log out page',
                'slug' => 'list-am-log-out-page'
            ),
            array(
                'title' => 'List.am user declarations',
                'slug' => 'list-am-user-declarations'
            ),
            array(
                'title' => 'List.am user profile',
                'slug' => 'list_am_user_profile'
            ),
            array(
                'title' => 'List.am login',
                'slug' => 'list-am-login'
            ),
            array(
                'title' => 'Declaration add categories',
                'slug' => 'declaration-add-categories'
            )
        );

        foreach ($auto_creation_pages as $page) {
            $page_exists = get_page_by_title($page['title']);
            if (is_null($page_exists)) {
                wp_insert_post(
                    array(
                        'post_type' => 'page',
                        'post_title' => $page['title'],
                        'post_status' => 'publish',
                        'comment_status' => 'closed',
                        'post_name' => $page['slug']
                    ));
            }
        }
    }

    public function create_settings_page()
    {
        $hook = add_options_page('Custom List.am', 'Custom List.am settings', 'manage_options', 'listam', array($this, 'create_post_type_form_fields'));

        //hide add New from admin of custom post type
        $all_custom_post_types = get_post_types();
        unset($all_custom_post_types['post'], $all_custom_post_types['page'], $all_custom_post_types['revision'], $all_custom_post_types['attachment'],
            $all_custom_post_types['nav_menu_item']);

        global $submenu;
        foreach ($all_custom_post_types as $custom_post_type) {
            unset($submenu['edit.php?post_type=' . $custom_post_type][10]);

            // Hide link on listing page
            if (isset($_GET['post_type']) && $_GET['post_type'] == $custom_post_type) {
                echo '<style type="text/css">
    #favorite-actions, .add-new-h2, .tablenav { display:none; }
    </style>';
            }
        }


    }

    public function register_post_type_form_fields()
    {
        //list.am post type fields
        register_setting('listam-group', 'post_type_name');
        register_setting('listam-group', 'post_type_description');
        register_setting('listam-group', 'post_type_menu_position');
        register_setting('listam-group', 'post_type_menu_icon');
        register_setting('listam-group', 'post_type_parent_category');

        //list.am post type input fields
        register_setting('listam-field-group', 'post_type_field_name');
        register_setting('listam-field-group', 'post_type_field_type');
        register_setting('listam-field-group', 'post_type_field_content');
        register_setting('listam-field-group', 'post_type_name_for_field');
        register_setting('listam-field-group', 'show_post_type_field_name');

    }

    public function create_post_type_form_fields()
    {
        ?>
        <div class="wrap">
            <?php
                include 'includes/views/custom_post_type.php';
                include 'includes/views/filter.php';
                include 'includes/views/custom_post_type_additional_field.php';
            ?>
        </div>
        <?php
    }

    public function register_custom_post_type()
    {
        $this->do_on_custom_post_type_save();
        $data = $this->dbObject->findAll('list_am_post_types');
        foreach ($data as $item) {
            $custom_args = array(
                'description' => $item->description,
                'menu_position' => intval($item->menu_position),
                'menu_icon' => $item->menu_icon,
                'rewrite' => array(
                    'slug' => get_category(get_cat_ID($item->label))->slug
                ),
                'labels' => array(
                    'name' => $item->label,
                    'singular_name' => __($item->label, $item->label),
                    'add_new' => 'Add New',
                    'add_new_item' => 'Add New ' . $item->label . ' Item',
                    'new_item' => 'New ' . $item->label . ' Item',
                    'edit_item' => 'Edit ' . $item->label . ' Item',
                    'view_item' => 'View ' . $item->label . ' Item',
                    'all_items' => 'All ' . $item->label . ' Items',
                    'search_items' => 'Search ' . $item->label . ' Items',
                    'not_found' => 'No ' . $item->label . ' items found.',
                    'not_found_in_trash' => 'No ' . $item->label . ' items found in Trash.'
                )
            );

            $all_args = array_merge($this->_default_args, $custom_args);
            register_post_type($item->label, $all_args);
        }
    }

    public function delete_and_reload($table, $post_type_id)
    {
        if ($table == 'list_am_post_types') {
            $post_type_data = $this->dbObject->get_post_type_label_name($post_type_id);
            $this->dbObject->delete_post_type_fields($post_type_data->name);
            wp_delete_category(get_cat_ID($post_type_data->label));
        }
        $this->dbObject->delete_by_id($table, $post_type_id);
        echo '<script>location.href = "http://localhost/wordpress/wp-admin/options-general.php?page=listam"</script>';
    }

    private function do_on_custom_post_type_save()
    {
        if (isset($_POST['submit']) && $_POST['submit']) {
            // insert/update <custom post types>
            if (isset($_POST['post_type_name']) && !empty($_POST['post_type_name'])) {
                $post_type_name = $_POST['post_type_name'];
                $post_type_label = $_POST['post_type_label'];
                $post_type_desc = $_POST['post_type_description'] == '' ? '' : $_POST['post_type_description'];
                $post_type_menu_pos = intval($_POST['post_type_menu_position']);
                $post_type_menu_icon = $_POST['post_type_menu_icon'] == '' ? 'dashicons-universal-access' : $_POST['post_type_menu_icon'];
                $post_type_parent_category = $_POST['post_type_parent_category'];
                $post_type_data = array(
                    'name' => $post_type_name,
                    'label'=>$post_type_label,
                    'description' => $post_type_desc,
                    'menu_position' => $post_type_menu_pos,
                    'menu_icon' => $post_type_menu_icon,
                    'parent_category' => $post_type_parent_category
                );
                if (isset($_POST['post_type_id'])){
                    $post_type_data['id'] = $_POST['post_type_id'];
                }

                $this->dbObject->save_post_type_data($post_type_data);

                wp_create_category($post_type_label, $post_type_parent_category);
            }

            //insert/update <custom post type additional fields>
            if (isset($_POST['post_type_field_name'])) {
                //save post type custom field
                $post_type_field_name = $_POST['post_type_field_name'];
                $post_type_field_type = $_POST['post_type_field_type'];
                $post_type_field_label = $_POST['post_type_field_label'];
                $post_type_field_content = '';
                if ($post_type_field_type != 'range' && $post_type_field_type != 'textarea') {
                    foreach ($_POST as $post_key => $post_value) {
                        if (strpos($post_key, 'postfield_content_item_') !== false) {
                            $post_type_field_content .= $post_value . ',';
                        }
                    }
                    $post_type_field_content = substr($post_type_field_content, 0, strlen($post_type_field_content) - 1);

                    if ($post_type_field_type == 'select') {
                        //add any option to select
                        $post_type_field_content = 'Any,' . $post_type_field_content;
                    } else if ($post_type_field_type == 'radio') {
                        //add None option to radio buttons
                        $post_type_field_content = 'None,' . $post_type_field_content;
                    }
                }
                $post_type_name_for_field = $_POST['post_type_name_for_field'];
                $show_field_name = $_POST['show_post_type_field_name'];
                $posttype_addfield_data = array(
                    'field_name' => $post_type_field_name,
                    'label'=>$post_type_field_label,
                    'filter_name' => $post_type_field_type,
                    'field_content' => $post_type_field_content,
                    'post_type_name' => $post_type_name_for_field,
                    'show_field_name' => $show_field_name
                );
                if (isset($_POST['post_type_addfield_id'])){
                    $posttype_addfield_data['id'] = $_POST['post_type_addfield_id'];
                }
                $this->dbObject->save_post_type_additional_fields($posttype_addfield_data);
            }

            //insert/update <custom filters>
            if (isset($_POST['filter_name'])) {
                $filter_name = $_POST['filter_name'];
                $filter_label = isset($_POST['filter_label']) ? $_POST['filter_label'] : "";
                $filter_data = array(
                    'name' => $filter_name,
                    'label' => $filter_label
                );
                if (isset($_POST['filter_id']) && !empty($_POST['filter_id'])) {
                    $filter_data['id'] = $_POST['filter_id'];
                }
                $this->dbObject->save_list_am_filter($filter_data);
            }

        }
    }

}

$list_am_post_type_object = new ListAm_post_type();

if (isset($_GET['remove'], $_GET['post_type_item']) && !empty($_GET['remove']) && !empty($_GET['post_type_item'])) {
    $list_am_post_type_object->delete_and_reload('list_am_post_types', $_GET['post_type_item']);
}

if (isset($_GET['remove'], $_GET['post_type_field']) && !empty($_GET['remove']) && !empty($_GET['post_type_field'])) {
    $list_am_post_type_object->delete_and_reload('list_am_post_type_fields', $_GET['post_type_field']);
}

if (isset($_GET['remove'], $_GET['filter_id']) && !empty($_GET['remove']) && !empty($_GET['filter_id'])) {
    $list_am_post_type_object->delete_and_reload('list_am_filters', $_GET['filter_id']);
}

register_activation_hook(__FILE__, array($list_am_post_type_object, 'on_plugin_activation'));
add_action('admin_menu', array($list_am_post_type_object, 'create_settings_page'));
add_action('admin_init', array($list_am_post_type_object, 'register_post_type_form_fields'));
add_action('init', array($list_am_post_type_object, 'register_custom_post_type'));
add_action('admin_enqueue_scripts', 'load_listam_admin_styles_and_scripts');
add_action('widgets_init', function () {
    register_widget('List_am_category_widget');
    register_widget('List_am_filters_widget');
});