<form action="options.php" method="post">
    <a class="list_am_back">Back to</a>
    <?php settings_fields('listam-field-group'); ?>
    <table class="form-table">
        <tbody>
        <tr style="display: none;">
            <td>
                <input type="hidden" id="post_type_addfield_id" name="post_type_addfield_id" class="custom_post_form_field"
                       required="true" value="<?php echo get_option('filter_id'); ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_field_name">Post type field name</label></td>
            <td>
                <input type="text" id="post_type_field_name" name="post_type_field_name"
                       class="custom_post_form_field"
                       required="true" value="<?php echo get_option('post_type_field_name'); ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_field_label">Post type field label</label></td>
            <td>
                <input type="text" id="post_type_field_label" name="post_type_field_label"
                       class="custom_post_form_field"
                       required="true" value="<?php echo get_option('post_type_field_label'); ?>"/>
                <p class="description" id="tagline-description" style="display: inline-block;">
                    Will be used in code(only underscores and lowercase letters)</p>
            </td>
        </tr>

        <tr>
            <td><label for="post_type_field_type">Post type field type</label></td>
            <td>
                <select name="post_type_field_type" id="post_type_field_type"
                        class="custom_post_form_field" onchange="changeFieldContentApp()">
                    <?php
                    $list_am_filter_types = $this->dbObject->get_filters_for_dropdown();
                    foreach ($list_am_filter_types as $filter_obj) {
                        ?>
                        <option
                            value="<?= $filter_obj->name ?>"><?= $filter_obj->label; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr id="field_content_section">
            <td><label for="post_type_field_content">Post type field content</label></td>
            <td>
                <div class="custom_post_form_field" id="content_items">

                </div>
                <button style="display: inline-block;margin-top:10px;cursor: pointer;"
                        id="add_content_item">Add
                </button>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_name_for_field">Post type</label></td>
            <td>
                <select name="post_type_name_for_field" id="post_type_name_for_field"
                        class="custom_post_form_field">
                    <?php
                    $post_types = $this->dbObject->findAll('list_am_post_types');
                    foreach ($post_types as $item) {
                        echo "<option value = '" . $item->name . "'>" . $item->label . "</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><label for="show_post_type_field_name">Show Field name in front-end</label></td>
            <td>
                <select name="show_post_type_field_name" id="show_post_type_field_name"
                        class="custom_post_form_field">
                    <option value="1">True</option>
                    <option value="0">False</option>
                </select>
            </td>
        </tr>
        </tbody>
    </table>
    <?php @submit_button(); ?>
</form>
