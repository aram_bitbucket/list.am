<form action="options.php" method="post">
    <a class="list_am_back">Back to</a>
    <?php settings_fields('listam-group'); ?>
    <table class="form-table">
        <tbody>
        <tr style="display: none;">
            <td>
                <input type="hidden" id="post_type_id" name="post_type_id" class="custom_post_form_field"
                       required="true" value="<?php echo get_option('filter_id'); ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_name">Post type name</label></td>
            <td>
                <input type="text" id="post_type_name" name="post_type_name"
                       class="custom_post_form_field"
                       required="true" value="<?php echo get_option('post_type_name'); ?>"/>
                <p class="description" id="tagline-description" style="display: inline-block;">
                    Will be used in code(only underscores and lowercase letters)
                    You can
                    not
                    type page, post, attachment,
                    revision, nav_menu_item here they are used by Wordpress</p>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_label">Post type label</label></td>
            <td>
                <input type="text" id="post_type_label" name="post_type_label"
                       class="custom_post_form_field"
                       required="true" value="<?php echo get_option('post_type_label'); ?>"/>
                <p class="description" id="tagline-description" style="display: inline-block;">You can
                    not
                    type page, post, attachment,
                    revision, nav_menu_item here they are used by Wordpress</p>
            </td>
        </tr>
        <tr>
            <td style="font-size: 12px;">Note: This value will be visible in front-end of application
                and
                will describe the whole post type
            </td>
        </tr>
        <tr>
            <td><label for="post_type_description">Post type description</label></td>
            <td>
                            <textarea name="post_type_description" id="post_type_description" cols="30"
                                      rows="5"
                                      class="custom_post_form_field"><?php echo get_option('post_type_description') ?></textarea>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_menu_position">Post type position in menu</label></td>
            <td>
                <select name="post_type_menu_position" id="post_type_menu_position"
                        class="custom_post_form_field">
                    <?php
                    global $custom_post_type_menu_positions;
                    foreach ($custom_post_type_menu_positions as $pos_key => $pos_value) {
                        ?>
                        <option value="<?= $pos_key ?>"><?= $pos_value; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_menu_icon">Post type Menu Icon</label></td>
            <td>
                <input type="text" id="post_type_menu_icon" name="post_type_menu_icon"
                       class="custom_post_form_field"
                       value="<?php echo get_option('post_type_menu_icon'); ?>"/>

                <p class="description" id="tagline-description" style="display: inline-block;">You can
                    take
                    menu icon from <a
                        href="https://developer.wordpress.org/resource/dashicons" target="_blank">this
                        website</a></p>
            </td>
        </tr>
        <tr>
            <td><label for="post_type_parent_category">Post type parent category</label></td>
            <td>
                <select name="post_type_parent_category" id="post_type_parent_category">
                    <?php
                    $args = array(
                        'type' => 'post',
                        'hide_empty' => 0
                    );

                    if (isset($post_type_name_IDs)) {
                        $args['exclude'] = $post_type_name_IDs;
                    }

                    $categories = get_categories($args);
                    foreach ($categories as $category) {
                        if ($category->name != 'Uncategorized')
                            echo '<option value = "' . $category->cat_ID . '">' . $category->name . '</option>';
                    }
                    ?>
                </select>

            </td>
        </tr>
        </tbody>
    </table>
    <?php @submit_button(); ?>
</form>
