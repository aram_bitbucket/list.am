<form action="options.php" method="post">
    <a class="list_am_back">Back to</a>
    <?php settings_fields('listam-group'); ?>
    <table class="form-table">
        <tbody>
        <tr style="display: none;">
            <td>
                <input type="hidden" id="filter_id" name="filter_id" class="custom_post_form_field"
                       required="true" value="<?php echo get_option('filter_id'); ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="filter_name">Filter name</label></td>
            <td>
                <input type="text" id="filter_name" name="filter_name" class="custom_post_form_field"
                       required="true" value="<?php echo get_option('filter_name'); ?>"/>

                <p class="description" id="tagline-description" style="display: inline-block;">
                    Will be used in urls(must contain only lowercase letters and underscore)</p>
            </td>
        </tr>
        <tr>
            <td><label for="filter_label">Filter label</label></td>
            <td>
                <input type="text" id="filter_label" name="filter_label" class="custom_post_form_field"
                       required="true" value="<?php echo get_option('filter_label'); ?>"/>

                <p class="description" id="tagline-description" style="display: inline-block;">
                    Will be used in different parts of website for users(arbitary string)</p>
            </td>
        </tr>
        </tbody>
    </table>
    <?php @submit_button(); ?>
</form>
