<section class="custom_post_type_additional_field" style="margin-top: 60px;">
    <h2>List.am custom post type additional field CRUD</h2>
    <a class="add_new_post_type">Add New Post type field</a>
    <?php
    $post_type_field_data = $this->dbObject->findAll('list_am_post_type_fields');
    if (!empty($post_type_field_data)) {
        ?>
        <table class="list_am_tbl">
            <thead>
            <tr>
                <th>Number</th>
                <th>Label</th>
                <th>Name</th>
                <th>Type</th>
                <th>Content</th>
                <th>Field's post type</th>
                <th>Show field name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($post_type_field_data as $item) {
                echo '<tr>';
                echo '<td>' . $item->id . '</td>';
                echo '<td>' . $item->label . '</td>';
                echo '<td>' . $item->field_name . '</td>';
                echo '<td>' . $this->dbObject->get_filter_label_by_name($item->filter_name) . '</td>';
                echo '<td>' . $item->field_content . '</td>';
                echo '<td>' . $this->dbObject->get_post_type_label_by_name($item->post_type_name) . '</td>';
                echo '<td>' . (($item->show_field_name == '1')?"true":"false") . '</td>';
                echo '<td><a class = "dashicons-edit post_type_edit"></a></td>';
                echo '<td><a class = "dashicons-trash post_type_delete" href = "' . get_site_url() . '/wp-admin/options-general.php?page=listam&post_type_field=' . $item->id . '&remove=true' . '"></a></td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <?php
    } else {
        ?>
        <p class="no_post_types">There are no additional fields for custom post types</p>
        <?php
    }
    include plugin_dir_path(__DIR__).'/forms/custom_post_type_additional_field.php';
    ?>
</section>
