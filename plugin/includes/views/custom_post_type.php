<section class="custom_post_type_crud">
    <h2>List.am custom post type CRUD</h2>
    <a class="add_new_post_type">Add New Post
        Type</a>
    <?php
    $post_type_data = $this->dbObject->findAll('list_am_post_types');
    //data table
    if (!empty($post_type_data)) {
        foreach ($post_type_data as $item) {
            $post_type_names[] = get_cat_ID($item->name);
        }
        $post_type_name_IDs = implode(',', $post_type_names);
        ?>
        <table class="list_am_tbl">
            <thead>
            <tr>
                <th>Number</th>
                <th>Name</th>
                <th>Label</th>
                <th>Description</th>
                <th>Menu position</th>
                <th>Menu icon</th>
                <th>Parent category</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($post_type_data as $item) {
                echo '<tr>';
                echo '<td>'.$item->id.'</td>';
                echo '<td>' . $item->name . '</td>';
                echo '<td>' . $item->label . '</td>';
                echo '<td>' . $item->description . '</td>';
                echo '<td>' . $item->menu_position . '</td>';
                echo '<td>' . $item->menu_icon . '</td>';
                echo '<td>' . get_cat_name($item->parent_category) . '</td>';
                echo '<td><a class = "dashicons-edit post_type_edit"></a></td>';
                echo '<td><a class = "dashicons-trash post_type_delete" href = "' . get_site_url() . '/wp-admin/options-general.php?page=listam&post_type_item=' . $item->id . '&remove=true' . '"></a></td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    <?php } else { ?>
        <p class="no_post_types">There are no custom post types</p>
    <?php }
    //data form
    include plugin_dir_path(__DIR__).'/forms/custom_post_type.php';
    ?>
</section>