<section class="list_am_filters">
    <h2>List.am Filters</h2>
    <a class="add_new_post_type">Add New Filter</a>
    <?php
    $post_type_data = $this->dbObject->findAll('list_am_filters');
    if (!empty($post_type_data)) {
        ?>
        <table class="list_am_tbl">
            <thead>
            <tr>
                <th>Number</th>
                <th>Name</th>
                <th>Label</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($post_type_data as $item) {
                echo '<tr>';
                echo '<td>' . $item->id . '</td>';
                echo '<td>' . $item->name . '</td>';
                echo '<td>' . $item->label . '</td>';
                echo '<td><a class = "dashicons-edit post_type_edit"></a></td>';
                echo '<td><a class = "dashicons-trash post_type_delete" href = "' . get_site_url() . '/wp-admin/options-general.php?page=listam&filter_id=' . $item->id . '&remove=true' . '"></a></td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    <?php } else { ?>
        <p class="no_post_types">There are no filters</p>
    <?php }
    include plugin_dir_path(__DIR__).'/forms/filter.php';?>
</section>